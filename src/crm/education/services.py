# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def dictfetchone(cursor):
    row = cursor.fetchone()
    if row is None:
        return False
    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, row))


def period_count(group_id):
    sql = f"""
      select array_agg(id) as periods
      from education_groupperiod
      where group_id = {group_id}
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(sql)
        items = dictfetchone(cursor)
    return items
