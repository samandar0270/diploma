from django.contrib import admin
from .models import GroupPeriod, Group

class GroupAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "course", "start_date", "end_date")

class PeriodAdmin(admin.ModelAdmin):
    list_display = ("id", "group", "start_date", "end_date", "order_month", "price")

admin.site.register(Group, GroupAdmin)
admin.site.register(GroupPeriod, PeriodAdmin)