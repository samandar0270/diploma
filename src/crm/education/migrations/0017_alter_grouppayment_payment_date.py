# Generated by Django 3.2 on 2021-07-03 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('education', '0016_auto_20210628_1233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouppayment',
            name='payment_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
