# Generated by Django 3.2 on 2021-05-20 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('education', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('active_status', models.SmallIntegerField(choices=[(1, 'Не активный'), (2, 'Активный'), (3, 'Удалено')], default=2)),
                ('name', models.CharField(max_length=100)),
                ('free', models.BooleanField(default=False)),
                ('start_time', models.TimeField(blank=True, null=True)),
                ('end_time', models.TimeField()),
            ],
            options={
                'verbose_name': 'Kundalik Grafik',
                'verbose_name_plural': 'Grafiklar',
            },
        ),
    ]
