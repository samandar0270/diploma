from datetime import datetime
from math import ceil
from dateutil import relativedelta
from django.db import transaction
from .models import GroupPeriod, GroupPayment
from .services import period_count


class PeriodManager(object):

    def createPeriod(self, group_model):
        start_date = group_model.start_date
        end_date = group_model.end_date
        # start_date = datetime.strptime(group_model.start_date, '%Y-%m-%d').date()
        # end_date = datetime.strptime(group_model.end_date, '%Y-%m-%d').date()
        count_months = self.get_number_months(start_date, end_date)
        with transaction.atomic():
            for i in range(1, count_months + 1):

                period_model = GroupPeriod(group=group_model, order_month=i, price=group_model.price_month)
                period_model.start_date = start_date
                if group_model.period_type == 1:
                    end_date = start_date + relativedelta.relativedelta(months=1)
                    period_model.end_date = end_date
                    start_date = end_date
                else:
                    end_date = self.last_date_of_month(start_date)
                    if i == 1:
                        diff_days = relativedelta.relativedelta(end_date, start_date)
                        if diff_days.days < 28:
                            per_price = ceil(group_model.price_month / 30) * abs(diff_days.days)
                            period_model.price = per_price

                    period_model.end_date = end_date
                    start_date = start_date + relativedelta.relativedelta(months=1, day=1)
                period_model.save()
            if group_model.end_date != end_date:
                period_model = GroupPeriod(group=group_model, order_month=i + 1, price=group_model.price_month)
                period_model.start_date = start_date
                period_model.end_date = group_model.end_date
                diff_days = relativedelta.relativedelta(group_model.end_date, start_date)
                per_price = ceil(group_model.price_month / 30) * abs(diff_days.days)
                period_model.price = per_price
                period_model.save()

    def last_date_of_month(self, dt):
        mydate = datetime(2021, 2, 26)
        last_date = datetime(mydate.year, dt.month, 1) + relativedelta.relativedelta(months=1, days=-1)
        return last_date

    def get_number_months(self, date1, date2):
        r = relativedelta.relativedelta(date2, date1)
        return r.months


class PaymentManager(object):

    def createGroupPayment(self, student_model, company,price_month):
        group_id = student_model.group_id
        periods = period_count(group_id)
        periods = periods['periods']
        with transaction.atomic():
            for i in periods:
                payment_model = GroupPayment(student_id=student_model.student_id, period_id=i, amount=0, payment_type=1,
                                             status=1,company_id=company, price_month=price_month)
                payment_model.save()
