from django.urls import include, path
from .geo.urls import urlpatterns as geo_urls
from .education.urls import urlpatterns as education_urls
from .company.urls import urlpatterns as company_urls
from .timesheets.urls import urlpatterns as timesheet_urls

urlpatterns = [
    path("geo/", include(geo_urls)),
    path("education/", include(education_urls)),
    path("company/", include(company_urls)),
    path("timesheet/", include(timesheet_urls)),
]
