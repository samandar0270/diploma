from django.urls import path
from .student.views import StudentView, ExpectedStudentView
from .profile.views import ProfileView
from .position.views import PositionView
from .teacher.views import TeacherView


urlpatterns = [
    path('teacher/<int:pk>/', TeacherView.as_view(), name='education_student_one'),
    path('teachers/', TeacherView.as_view(), name='education_student_one'),
    path('students/<int:pk>/', StudentView.as_view(), name='education_student_one'),
    path('students/', StudentView.as_view(), name='education_student_list'),
    path('expected_students/', ExpectedStudentView.as_view(), name='expected_students_list'),
    path('expected_student/<int:pk>/', ExpectedStudentView.as_view(), name='expected_student'),
    path('profile/', ProfileView.as_view(), name='education_profile_one'),
    path('profile/<int:pk>/', ProfileView.as_view(), name='education_profile_edit'),
    path('position/<int:pk>/', PositionView.as_view(), name='education_position_one'),
    path('position/', PositionView.as_view(), name='education_position_one'),

]
