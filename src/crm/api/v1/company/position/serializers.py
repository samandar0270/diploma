from rest_framework import serializers
from crm.company.models import Position


class PositionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        profile = Position(**validated_data)
        profile.save()
        return profile

    class Meta:
        model = Position
        exclude = ['id']
