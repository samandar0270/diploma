from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .services import list_positions, one_position
from .serializers import PositionSerializer
from crm.company.models import Position


class PositionView(GenericAPIView):

    permission_classes = (AllowAny,)
    serializer_class = PositionSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Position.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found position')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_position(request, kwargs['pk'])
        else:
            result = list_positions(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_position(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        position = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=position, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_position(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        position = self.get_object(*args, **kwargs)
        position.status = 3
        position.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
