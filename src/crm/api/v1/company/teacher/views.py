from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .services import teacher_data, list_teachers
from .serializers import TeacherSerializer
from crm.company.models import Member

class TeacherView(GenericAPIView):

    permission_classes = (AllowAny,)
    serializer_class = TeacherSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Member.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found teacher')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = teacher_data(request, kwargs['pk'])
        else:
            result = list_teachers(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

