from rest_framework import serializers
from crm.company.models import Member


class TeacherSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        validated_data['region_id'] = validated_data.pop("region")
        validated_data['district_id'] = validated_data.pop("district")

        teacher = Member(**validated_data)
        teacher.is_student = True
        teacher.save()
        return teacher

    class Meta:
        model = Member
        exclude = ['is_student', 'status']
