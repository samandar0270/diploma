# -*- coding: utf-8 -*-
from datetime import datetime
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def list_teachers(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE
    requests = request.query_params


    extra_sql = """
select teacher.id, coalesce(teacher.lastname, '') || ' ' || coalesce(teacher.firstname, '') as fullname
from company_member teacher 
where is_student = false
order by id asc
limit {} OFFSET {}
""".format(PER_PAGE, offset)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(t_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where is_student = False ")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])



def teacher_data(request, teacher_id):
    time = datetime.now()
    month = str(time.month)
    if len(month) == 2:
        month = month
    else:
        month = f'0{time.month}'

    month_id = str(month)

    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE


    extra_sql = """
select egroup.teacher_1_id as teacher_id, student.id as student_id, coalesce(student.lastname, '') || ' ' || coalesce(student.firstname, '') as student_name,
course."name" as course_name, egroup."name" as group_name, payment.payment_date, payment.amount, grperiod.price as price_month, (grperiod.price - payment.amount) as debt 
from education_grouppayment payment
inner join company_member student on payment.student_id = student.id
inner join education_groupperiod grperiod on payment.period_id = grperiod.id and to_char(grperiod.start_date, 'MM')='{}' 
inner join education_group egroup on grperiod.group_id = egroup.id
inner join education_course course on egroup.course_id = course.id
where egroup.teacher_1_id = {}
order by payment.payment_date desc
limit {} OFFSET {}
""".format(month_id,teacher_id,PER_PAGE, offset)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute("""
        SELECT count(1) as cnt FROM education_groupperiod where to_char( education_groupperiod.start_date, 'MM')='{}' 
        """.format(month_id))
        row = dictfetchone(cursor)



    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    teachers = teacher(teacher_id)
    teacher_name = OrderedDict([
        ('id', teachers['id']),
        ('fullname', teachers['fullname'])
    ])

    totall = total_items(teacher_id, month)
    total = OrderedDict([
        ('total_amount', totall['total_amount']),
        ('total_price', totall['total_price']),
        ('total_debt', totall['total_debt'])
    ])

    return OrderedDict([
        ('teacher', teacher_name),
        ('items', result),
        ('meta', pagging),
        ('total_summary', total)
    ])


def teacher(teacher_id):

    extra_sql = """
    select id, coalesce(lastname, '') || ' ' || coalesce(firstname, '') as fullname
    from company_member teacher 
    where is_student = false and id = {}
    """.format(teacher_id)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        data = dictfetchone(cursor)

    return data




def total_items(teacher_id, month):

    extra_sql = """
    select  sum(payment.amount) as total_amount, sum(grperiod.price) as total_price, sum(grperiod.price - payment.amount) as total_debt  
    from education_grouppayment payment
    inner join education_groupperiod grperiod on payment.period_id = grperiod.id and to_char(grperiod.start_date, 'MM')='{}' 
    inner join education_group egroup on grperiod.group_id = egroup.id
    where egroup.teacher_1_id ={}
    """.format(month,teacher_id)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        data = dictfetchone(cursor)

    return data



def t_format(data):

    return OrderedDict([
        ('id', data['id']),
        ('fullname', data['fullname'])
    ])

def _format(data):

    return OrderedDict([
        ('id', data['student_id']),
        ('fullname', data['student_name']),
        ('course_name', data['course_name']),
        ('group', data['group_name']),
        ('payment_date', data['payment_date']),
        ('payment_amount', data['amount']),
        ('price_month', data['price_month']),
        ('debt', data['debt'])
    ])




