# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator
from .....company import MemberChoice

PER_PAGE = settings.PAGINATE_BY






def expected_student_course(request, course_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
select student.id, coalesce(student.lastname, '') ||' '||coalesce(student.firstname, '') as fullname, student.phone_number, egroup."name" as group_name, region."name" as region
from company_member student 
left join geo_region region on student.region_id = region.id
inner join education_groupstudent grstudent on student.id = grstudent.student_id 
inner join education_group egroup on grstudent.group_id = egroup.id
where student.status = 4 and student.is_student = True and egroup.course_id = {}
order by student.id asc
limit {} OFFSET {}
""".format(course_id, PER_PAGE, offset)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(ex_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute("""
        SELECT count(1) as cnt 
        FROM company_member student
        inner join education_groupstudent grstudent on student.id = grstudent.student_id 
        inner join education_group egroup on grstudent.group_id = egroup.id
        where student.status = 4 and student.is_student = true and egroup.course_id ={}
        """.format(course_id))
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])







def list_students(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE
    requests = request.query_params
    where = ""
    if "fio" in requests and requests.get('fio'):
        where = f" and company_member.firstname ilike '%{requests.get('fio')}%'"


    extra_sql = """
SELECT company_member.id, company_member.firstname, company_member.lastname, company_member.middlename, 
company_member.status, 
company_member.email, company_member.phone_number, company_member.birthday, company_member.gender, 
company_member.join_date, company_member.address, company_member.pass_serial, 
coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
geo_region.id as region_id, geo_region.name as region_name, geo_district.id as district_id, 
geo_district.name as district_name
FROM company_member
left join geo_region on company_member.region_id = geo_region.id
left join geo_district on company_member.district_id = geo_district.id
where company_member.status = 1 or company_member.status = 2 and company_member.is_student = true{}
order by join_date desc
limit {} OFFSET {}
""".format(where, PER_PAGE, offset)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 2 and is_student = true{}".format(where))
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])




def expected_students(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE


    extra_sql = """
select student.id, status,
coalesce(student.lastname, '') ||' '||coalesce(student.firstname, '') as fullname, student.phone_number, egroup."name" as group_name, region."name" as region
from company_member student 
left join geo_region region on student.region_id = region.id
left join education_groupstudent grstudent on student.id = grstudent.student_id 
left join education_group egroup on grstudent.group_id = egroup.id
where student.status = 4 and student.is_student = True
order by student.id asc
limit {} OFFSET {}
""".format(PER_PAGE, offset)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(ex_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 4 and is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])





def one_students(request, student_id):

    extra_sql = """
SELECT company_member.id, company_member.firstname, company_member.lastname, company_member.middlename, 
company_member.email, company_member.phone_number, company_member.birthday, company_member.gender,
company_member.status, 
company_member.join_date, company_member.address, company_member.pass_serial, 
coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
geo_region.id as region_id, geo_region.name as region_name, geo_district.id as district_id, 
geo_district.name as district_name
FROM company_member
left join geo_region on company_member.region_id = geo_region.id
left join geo_district on company_member.district_id = geo_district.id
where company_member.id = %s

"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id])
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])

def _format(data):

    if data['region_id']:
        region = OrderedDict([
            ('id', data['region_id']),
            ('name', data['region_name'])
        ])
    else:
        region = None

    if data['district_id']:
        district = OrderedDict([
            ('id', data['district_id']),
            ('name', data['district_name'])
        ])
    else:
        district = None

    if data['status']:
        status = OrderedDict([
            ('id', data['status']),
            ('name', MemberChoice.getValue(data['status']))
        ])
    else:
        status = None

    if data['gender']:
        gender = 'male'
    else:
        gender = 'female'

    return OrderedDict([
        ('id', data['id']),
        ('firstname', data['firstname']),
        ('lastname', data['lastname']),
        ('middlename', data['middlename']),
        ('email', data['email']),
        ('phone_number', data['phone_number']),
        ('birthday', data['birthday']),
        ('gender', gender),
        ('join_date', data['join_date']),
        ('address', data['address']),
        ('pass_serial', data['pass_serial']),
        ('full_name', data['full_name']),
        ("region", region),
        ("district", district),
        ("status", status),
    ])


def ex_format(data):
    if data['status']:
        status = OrderedDict([
            ('id', data['status']),
            ('name', MemberChoice.getValue(data['status']))
        ])
    else:
        status = None
    return OrderedDict([
        ('id', data['id']),
        ('student_name', data['fullname']),
        ('phone_number', data['phone_number']),
        ("region", data['region']),
        ("group", data['group_name']),
        ("status", status)
    ])


