from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .services import list_students, one_students, expected_student_course, expected_students
from .serializers import StudentSerializer
from crm.company.models import Member

class StudentView(GenericAPIView):

    permission_classes = (AllowAny,)
    serializer_class = StudentSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Member.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found student')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_students(request, kwargs['pk'])
        else:
            result = list_students(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_students(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        student = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=student, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_students(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        student = self.get_object(*args, **kwargs)
        student.status = 3
        student.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)




class ExpectedStudentView(GenericAPIView):

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = expected_student_course(request, kwargs['pk'])
        else:
            result = expected_students(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

