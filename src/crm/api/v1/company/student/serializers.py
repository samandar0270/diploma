from rest_framework import serializers
from crm.company.models import Member


class StudentSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        validated_data['region_id'] = validated_data.pop("region")
        validated_data['district_id'] = validated_data.pop("district")

        student = Member(**validated_data)
        student.is_student = True
        student.save()
        return student

    class Meta:
        model = Member
        fields = "__all__"
