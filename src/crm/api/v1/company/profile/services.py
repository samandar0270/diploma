# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone

PER_PAGE = settings.PAGINATE_BY


def see_profile():
    sql = """
SELECT company_profile.id, company_profile.title, company_profile.email, company_profile.description,
company_profile.phone_number, company_profile.logo, company_profile.lat, 
company_profile.lng, company_profile.address,
geo_region.id as region_id, geo_region.name as region_name, geo_district.id as district_id, 
geo_district.name as district_name
FROM company_profile 
left join geo_region on company_profile.region_id = geo_region.id
left join geo_district on company_profile.district_id = geo_district.id
order by company_profile.id desc
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(sql)
        item = dictfetchone(cursor)
        result = _format(item)

    return OrderedDict([
        ('items', result),

    ])


def _format(data):

    if data['region_id']:
        region = OrderedDict([
            ('id', data['region_id']),
            ('name', data['region_name'])
        ])
    else:
        region = None

    if data['district_id']:
        district = OrderedDict([
            ('id', data['district_id']),
            ('name', data['district_name'])
        ])
    else:
        district = None

    return OrderedDict([
        ('id', data['id']),
        ('title', data['title']),
        ('description', data['description']),
        ('email', data['email']),
        ('logo', data['logo']),
        ('phone_number', data['phone_number']),
        ('lat', data['lat']),
        ('lng', data['lng']),
        ('address', data['address']),
        ("region", region),
        ("district", district),
    ])


