from rest_framework import serializers
from crm.company.models import Profile


class ProfileSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        validated_data['region_id'] = validated_data.pop("region")
        validated_data['district_id'] = validated_data.pop("district")

        profile = Profile(**validated_data)
        profile.save()
        return profile

    class Meta:
        model = Profile
        exclude = ['description']
