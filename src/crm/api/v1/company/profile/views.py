from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .services import see_profile
from .serializers import ProfileSerializer
from crm.company.models import Profile


class ProfileView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ProfileSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Profile.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found profile')
        return product

    def get(self, request, *args, **kwargs):
        result = see_profile()
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = see_profile()
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        profile = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=profile, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = see_profile()
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')
