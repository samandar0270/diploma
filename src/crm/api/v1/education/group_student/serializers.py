from rest_framework import serializers
from crm.education.models import GroupStudent


class GrStudentSerializer(serializers.ModelSerializer):
    def create(self, validated_data):

        validated_data['student_id'] = validated_data.pop("student")
        validated_data['group_id'] = validated_data.pop("group")

        root = GroupStudent(**validated_data)
        root.save()
        return root

    class Meta:
        model = GroupStudent
        exclude = ['id']
