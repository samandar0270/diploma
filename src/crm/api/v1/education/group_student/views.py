from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import GroupStudent
from .services import one_grstudent, list_grstudent
from .serializers import GrStudentSerializer


class GrStudentView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = GrStudentSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = GroupStudent.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found group_student')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_grstudent(request, grstudent_id=kwargs['pk'])
        elif "student_id" in kwargs and kwargs["student_id"]:
            result = one_grstudent(request, student_id=kwargs['student_id'])
        elif "group_id" in kwargs and kwargs["group_id"]:
            result = one_grstudent(request, edugroup_id=kwargs['group_id'])
        else:
            result = list_grstudent(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        print(kwargs)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_grstudent(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        group = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=group, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_grstudent(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        self.get_object(*args, **kwargs).delete()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
