# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def list_grstudent(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
       select gr_student.id , gr_student.group_id, gr_student.start_date, gr_student.end_date,
       edu_group.start_time, edu_group.end_time,
       gr_student.active_status, 
        gr_student.student_id, gr_student.group_id, edu_group.name as group_name, 
        coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student
        from education_groupstudent gr_student 
        inner join education_group edu_group on gr_student.group_id = edu_group.id 
        inner join company_member student on gr_student.student_id = student.id
    
        where gr_student.active_status = 1 or gr_student.active_status = 2
        order by gr_student.start_date desc
            
        limit %s OFFSET %s
        """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_groupstudent where active_status = 1 or active_status = 2")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_grstudent(request, grstudent_id=None, student_id=None, edugroup_id=None):
    extra_sql = """
    select gr_student.id , gr_student.group_id, edu_group.start_date, edu_group.end_date, edu_group.start_time, edu_group.end_time,
    gr_student.student_id, gr_student.group_id, edu_group.name as group_name,
    coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student
    from education_groupstudent gr_student 
    inner join education_group edu_group on gr_student.group_id = edu_group.id 
    inner join company_member student on gr_student.student_id = student.id
    """
    with closing(connection.cursor()) as cursor:
        if grstudent_id:
            extra_sql += "\nwhere gr_student.id = %s and (edu_group.active_status = 1 or gr_student.active_status = 2)"
            cursor.execute(extra_sql, [grstudent_id])
        elif student_id:
            extra_sql += "\nwhere student.id = %s and (edu_group.active_status = 1 or gr_student.active_status = 2)"
            cursor.execute(extra_sql, [student_id])
            items = dictfetchall(cursor)
            result = []
            for data in items:
                result.append(_format(data))

            return OrderedDict([
                ('item', result),
            ])
        elif edugroup_id:
            extra_sql += "\nwhere edu_group.id = %s and (edu_group.active_status = 1 or gr_student.active_status = 2)"
            cursor.execute(extra_sql, [edugroup_id])
            items = dictfetchall(cursor)
            result = []
            for data in items:
                result.append(_format(data))

            return OrderedDict([
                ('item', result),
            ])
            
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format(data):
    if "start_time" in data and data['start_time']:
        print(data['start_time'])
    else:
        print(data['group_id'])
    if data['group_id']:
        group = OrderedDict([
            ('id', data['group_id']),
            ('group_name', data['group_name']),
            ('start_date', data['start_date']),
            ('end_date', data['end_date']),
            ('start_time', data['start_time']),
            ('end_time', data['end_time']),

        ])
    else:
        group = None

    if data['student_id']:
        student = OrderedDict([
            ('id', data['student_id']),
            ('name', data['student'])
        ])
    else:
        student = None

    return OrderedDict([
        ('id', data['id']),
        ('group', group),
        ('student', student),
    ])
