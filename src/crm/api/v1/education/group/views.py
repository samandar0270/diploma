from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import Group
from .services import list_group, one_group
from .serializers import GroupSerializer


class GroupView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = GroupSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Group.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found group')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_group(request, kwargs['pk'])
        else:
            result = list_group(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_group(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        group = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=group, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_group(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        group = self.get_object(*args, **kwargs)
        group.active_status = 3
        group.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
