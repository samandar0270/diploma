# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def list_group(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
       SELECT edu_group.id, edu_group.name, edu_group.start_date,edu_group.end_date,
        edu_group.start_time,edu_group.end_time, edu_group.active_status, 
        edu_group.price_month, edu_group.teacher_1_id as teacher1_id, edu_group.teacher_2_id as teacher2_id,
        coalesce(teacher1.lastname, '') ||' ' ||coalesce(teacher1.firstname, '') as teacher_1,
        coalesce(teacher2.lastname, '') ||' ' ||coalesce(teacher2.firstname, '') as teacher_2
        FROM education_group edu_group
        inner join company_member teacher1 on edu_group.teacher_1_id = teacher1.id
        inner join company_member teacher2 on edu_group.teacher_2_id = teacher2.id
        
        
        where edu_group.active_status = 1 or edu_group.active_status = 2
        order by edu_group.start_date desc
            
        limit %s OFFSET %s
        """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_group where active_status = 1 or active_status = 2")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_group(request, group_id):
    extra_sql = """
   SELECT edu_group.id, edu_group.name, edu_group.start_date,edu_group.end_date,
        edu_group.start_time,edu_group.end_time, edu_group.active_status, 
        edu_group.price_month, edu_group.teacher_1_id as teacher1_id, edu_group.teacher_2_id as teacher2_id,
        coalesce(teacher1.lastname, '') ||' ' ||coalesce(teacher1.firstname, '') as teacher_1,
        coalesce(teacher2.lastname, '') ||' ' ||coalesce(teacher2.firstname, '') as teacher_2
        FROM education_group edu_group
        inner join company_member teacher1 on edu_group.teacher_1_id = teacher1.id
        inner join company_member teacher2 on edu_group.teacher_2_id = teacher2.id
        
    where edu_group.id = %s and (edu_group.active_status = 1 or edu_group.active_status = 2)
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [group_id])
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format(data):
    if data['teacher_1']:
        teacher_1 = OrderedDict([
            ('id', data['teacher1_id']),
            ('name', data['teacher_1'])
        ])
    else:
        teacher_1 = None

    if data['teacher_2']:
        teacher_2 = OrderedDict([
            ('id', data['teacher2_id']),
            ('name', data['teacher_2'])
        ])
    else:
        teacher_2 = None

    return OrderedDict([
        ('id', data['id']),
        ('name', data['name']),
        ('active_status', data['active_status']),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('start_time', data['start_time']),
        ('end_time', data['end_time']),
        ('price_month', data['price_month']),
        ('teacher_1', teacher_1),
        ('teacher_2', teacher_2),
    ])
