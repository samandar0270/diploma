from rest_framework import serializers
from crm.education.models import Group


class GroupSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['teacher_1_id'] = validated_data.pop("teacher_1")
        validated_data['teacher_2_id'] = validated_data.pop("teacher_2")

        group = Group(**validated_data)
        group.save()
        return group

    class Meta:
        model = Group
        exclude = ['id']
