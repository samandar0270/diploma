# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from ....base.utils.db import dictfetchall
from ....base.utils.sqlpaginator import SqlPaginator

def get_group_period_list(request, group_id):
    rows = _get_group_period(group_id)
    result = []
    for data in rows:
        result.append(OrderedDict([
            ('id', data['id']),
            ('name', f"{data['start_date']} || {data['end_date']}"),
            ('start_date', data['start_date']),
            ('end_date', data['end_date']),
            ('price', data['price']),
            ('order_month', data['order_month']),
        ]))
    paginator = SqlPaginator(request, page=1, per_page=len(result), count=len(result))
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])

def _get_group_period(group_id):
    extra_sql = "select * FROM education_groupperiod WHERE group_id = %s ORDER BY order_month LIMIT 20"
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [group_id])
        rows = dictfetchall(cursor)
    return rows
