from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import GroupPayment
from .services import one_grpayment, list_grpayment
from .serializers import GrPaymentSerializer


class GrPaymentView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = GrPaymentSerializer

    def get_object(self, *args, **kwargs):
        try:
            if "period_id" in kwargs and kwargs["period_id"]:
                product = GroupPayment.objects.get(period_id=kwargs['period_id'])
            else:
                product = GroupPayment.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found group_payment')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_grpayment(request, root_id=kwargs['pk'])
        elif "st_id" in kwargs and kwargs["st_id"]:
            result = one_grpayment(request, st_id=kwargs['st_id'])
        elif "gr_id" in kwargs and kwargs["gr_id"]:
            result = list_grpayment(request, gr_id=kwargs['gr_id'])
        else:
            result = list_grpayment(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_grpayment(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        root = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=root, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        print(data.pk)
        result = one_grpayment(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        root = self.get_object(*args, **kwargs)
        root.active_status = 3
        root.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
