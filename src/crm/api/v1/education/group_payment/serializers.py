from rest_framework import serializers
from crm.education.models import GroupPayment


class GrPaymentSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['student_id'] = validated_data.pop("student")
        validated_data['period_id'] = validated_data.pop("period")
        root = GroupPayment(**validated_data)
        root.save()
        return root

    class Meta:
        model = GroupPayment
        exclude = ['id']
