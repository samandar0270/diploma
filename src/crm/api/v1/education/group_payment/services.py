# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator
from .....education import PaymentChoice
from .....payment import PaymentType

PER_PAGE = settings.PAGINATE_BY


def list_grpayment(request, gr_id=None):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    if gr_id:
        sql = f"where edu_group.id = {gr_id}"
    else:
        sql = ""

    extra_sql = f"""
        select payment.id, payment.amount, payment.payment_date, payment.payment_type, payment.student_id,payment.student_id, 
        payment.period_id, payment.status, edu_group.id as group_id, edu_group."name" as gr_name,
        coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student_name
        from education_grouppayment payment
        inner join company_member student on payment.student_id = student.id
        inner join education_groupperiod periods on payment.period_id = periods.id 
        inner join education_group edu_group on periods.group_id = edu_group.id
        {sql}
        order by payment.payment_date desc
        limit %s OFFSET %s
        """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_grouppayment")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_grpayment(request, root_id=None, st_id=None):
    extra_sql = """
        select payment.id, payment.amount, payment.payment_date, payment.payment_type, payment.student_id, 
        payment.period_id, payment.status,
        coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student_name
        from education_grouppayment payment
        inner join company_member student on payment.student_id = student.id
       
    """
    with closing(connection.cursor()) as cursor:
        if st_id:
            extra_sql += "\nwhere payment.student_id = %s"
            cursor.execute(extra_sql, [st_id])
            items = dictfetchall(cursor)
            result = []
            for data in items:
                result.append(_format(data))

            return OrderedDict([
                ('item', result),
            ])

        else:
            extra_sql += "\nwhere payment.id = %s"
            cursor.execute(extra_sql, [root_id])
            data = dictfetchone(cursor)
            if data:
                result = _format(data)
            else:
                result = None
            return OrderedDict([
                ('item', result),
            ])


def _format(data):
    if data['student_id']:
        student = OrderedDict([
            ('id', data['student_id']),
            ('name', data['student_name'])
        ])
    else:
        student = None

    if "group_id" in data and data['group_id']:
        group = OrderedDict([
            ('id', data['group_id']),
            ('name', data['gr_name'])
        ])
    else:
        group = None

    if data['payment_type']:
        payment_type = OrderedDict([
            ('id', data['status']),
            ('name', PaymentType.getValue(data["payment_type"]))
        ])
    else:
        payment_type = None
    if data['status']:
        status = OrderedDict([
            ('id', data['status']),
            ('name', PaymentChoice.getValue(data["status"]))
        ])
    else:
        status = None

    return OrderedDict([
        ('id', data['id']),
        ('amount', data['amount']),
        ('payment_date', data['payment_date']),
        ('payment_type', payment_type),
        ('status', status),
        ('period', data['period_id']),
        ('student', student),
        ('group', group),
    ])
