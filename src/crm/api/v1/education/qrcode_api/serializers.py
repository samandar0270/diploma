from datetime import datetime
from rest_framework import serializers
from crm.company.models import Member
from crm.timesheet.models import Times

class TimesSerializer(serializers.Serializer):
    student = serializers.IntegerField(required=True, min_value=1)
    action = serializers.ChoiceField(required=True, choices=[1, 2])

    def __init__(self, *args, **kwargs):
        self.student = kwargs.pop('student')
        super(TimesSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        id = attrs.get('student', 0)
        try:
            self.student = Member.objects.get(pk=id)
        except Member.DoesNotExist:
            raise serializers.ValidationError({'id': ['not found student']})

        return attrs

    def create(self, validated_data):
        validated_data = self.validated_data
        member_id = validated_data.get('student', 0)
        action = validated_data.get('action', 0)

        now_dt = datetime.now()
        current_date = now_dt.date()
        current_hours = now_dt.time()

        try:
            times_model = Times.objects.get(member_id=member_id, current_date=current_date)
        except Member.DoesNotExist:
            times_model = Times()
            times_model.member_id = member_id
            times_model.time_date = current_date
        if action == 1 and (not times_model or not times_model.in_time or times_model.in_time == ''):
            times_model.in_time = current_hours
            times_model.save()
        else:
            times_model.out_time = current_hours
            times_model.save()
        return times_model
