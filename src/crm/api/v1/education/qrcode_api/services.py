# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator
from .....education import PaymentChoice


def qr_code(request, student_id, date):
    extra_sql = """
    select edu_group."name" as group_name,  gr_period.end_date, payment.status, payment.amount, gr_period.*,
    student.id as student_id, edu_group.id as group_id,
    coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student
    from education_groupperiod gr_period
    inner join education_grouppayment payment on gr_period.id = payment.period_id and payment.student_id = %s
    inner join education_group edu_group on gr_period.group_id = edu_group.id
    inner join company_member student on payment.student_id = student.id
    where %s  between gr_period.start_date and gr_period.end_date
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id, date])
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format(data):
    if data['group_id']:
        group = OrderedDict([
            ('id', data['group_id']),
            ('group_name', data['group_name'])
        ])
    else:
        group = None

    if data['student_id']:
        student = OrderedDict([
            ('id', data['student_id']),
            ('name', data['student'])
        ])
    else:
        student = None

    if data['status']:
        status = OrderedDict([
            ('id', data['status']),
            ('name', PaymentChoice.getValue(data["status"]))
        ])
    else:
        status = None

    return OrderedDict([
        ('student', student),
        ('group', group),
        ('id', data['id']),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('order_month', data['order_month']),
        ('status', status),
        ('amount', data['amount']),
        ('price', data['price']),

    ])

