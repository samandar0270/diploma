from datetime import datetime

from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from .services import qr_code


class QrCodeView(GenericAPIView):
    def post(self, request, *args, **kwargs):
        date = datetime.now().strftime("%Y-%m-%d")
        result = qr_code(request, request.data["student_id"], date)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')
