from rest_framework import serializers
from crm.education.models import Course


class CourseSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        course = Course(**validated_data)
        course.save()
        return course

    class Meta:
        model = Course
        exclude = ['id']
