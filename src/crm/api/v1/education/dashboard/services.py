from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings

from .....base import StatusChoice
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def dashboard_data(request):
    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as courses from education_course")
        courses = dictfetchone(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as groups from education_group")
        groups = dictfetchone(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as students from company_member student  where student.is_student = True")
        students = dictfetchone(cursor)


    return OrderedDict([
        ('courses', courses['courses']),
        ('groups', groups['groups']),
        ('students', students['students'])
    ])



