from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from .services import dashboard_data


class DashboardView(GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        result = dashboard_data(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

