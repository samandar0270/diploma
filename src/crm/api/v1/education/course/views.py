from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import Course
from .services import list_courses, one_course
from .serializers import CourseSerializer


class CourseView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CourseSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Course.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found course')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_course(request, kwargs['pk'])
        else:
            result = list_courses(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_course(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        course = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=course, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_course(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        course = self.get_object(*args, **kwargs)
        course.active_status = 3
        course.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
