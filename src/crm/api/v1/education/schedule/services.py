# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def list_schedule(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT *
from education_schedule 
where active_status = 1 or active_status = 2
order by id desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_schedule where active_status = 1 or active_status = 2")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_schedule(request, schedule_id):
    extra_sql = """
SELECT *
from education_schedule
where id = %s and (active_status = 1 or active_status = 2)
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [schedule_id])
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format(data):
    return OrderedDict([
        ('id', data['id']),
        ('name', data['name']),
        ('free', data['free']),
        ('active_status', data['active_status']),
        ('start_time', data['start_time']),
        ('end_time', data['end_time']),

    ])
