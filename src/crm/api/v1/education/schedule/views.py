from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import Schedule
from .services import list_schedule, one_schedule
from .serializers import ScheduleSerializer


class ScheduleView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ScheduleSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Schedule.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found schedule')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_schedule(request, kwargs['pk'])
        else:
            result = list_schedule(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_schedule(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        schedule = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=schedule, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_schedule(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        schedule = self.get_object(*args, **kwargs)
        schedule.active_status = 3
        schedule.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
