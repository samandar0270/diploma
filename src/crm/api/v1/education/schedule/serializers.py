from rest_framework import serializers
from crm.education.models import Schedule


class ScheduleSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        schedule = Schedule(**validated_data)
        schedule.save()
        return schedule

    class Meta:
        model = Schedule
        exclude = ['id']
