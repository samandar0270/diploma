from django.urls import path

from .group_payment.views import GrPaymentView
from .group_period.views import GrPeriodView
from .qrcode_api.views import QrCodeView
from .views import GroupPeriodView
from .course.views import CourseView
from .schedule.views import ScheduleView
from .group.views import GroupView
from .group_student.views import GrStudentView
from .monitoring.views import MonitoringView
from .dashboard.views import DashboardView


urlpatterns = [
    path('period/<int:group>/list/', GroupPeriodView.as_view(), name='education_period_group_list'),
    path("course/", CourseView.as_view(), name="course_list"),
    path("course/<int:pk>/", CourseView.as_view(), name="course_one"),
    path("schedule/", ScheduleView.as_view(), name="schedule_list"),
    path("schedule/<int:pk>/", ScheduleView.as_view(), name="schedule_one"),
    path("group/", GroupView.as_view(), name="group_list"),
    path("group/<int:pk>/", GroupView.as_view(), name="group_one"),
    path("grstudent/", GrStudentView.as_view(), name="grstudent_list"),
    path("grstudent/<int:pk>/", GrStudentView.as_view(), name="grstudent_one"),
    path("studenttogroup/<int:student_id>/", GrStudentView.as_view(), name="group_using_student"),
    path("grouptostudent/<int:group_id>/", GrStudentView.as_view(), name="student_using_group"),
    path("grpayment/s/<int:gr_id>/", GrPaymentView.as_view(), name="grpayment_list-with-gr_id"),
    path("grpayment/", GrPaymentView.as_view(), name="grpayment_list"),
    path("grpayment/<int:pk>/", GrPaymentView.as_view(), name="grpayment_one"),
    path("grpayment/p/<int:period_id>/", GrPaymentView.as_view(), name="grpayment_one"),
    path("grtopayment/<int:st_id>/", GrPaymentView.as_view(), name="payment_using_student"),
    path("grperiod/", GrPeriodView.as_view(), name="grperiod_list"),
    path("grperiod/<int:pk>/", GrPeriodView.as_view(), name="grperiod_one"),
    path("mperiod/<int:m_id>/", GrPeriodView.as_view(), name="grperiod_one"),
    path("monitoring/", MonitoringView.as_view(), name="monitoring"),
    path("dashboard/", DashboardView.as_view(), name="dashboard"),
    path("qrcode/", QrCodeView.as_view(), name="qrcode")

]
