from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from .services import get_group_period_list

class GroupPeriodView(APIView):

    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        '''API for getting district list'''
        if kwargs['group']:
            try:
                result = get_group_period_list(request, kwargs['group'])
                return Response(result, status=status.HTTP_200_OK, content_type='application/json')
            except Exception as e:
                raise NotFound('invalid argument1')

