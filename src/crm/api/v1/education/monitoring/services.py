from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings

from .....base import StatusChoice
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def monitoring_data(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT student.id as student_id, student.lastname, student.firstname, 
grperiod.start_date, grperiod.end_date, payment.amount, payment.payment_date, payment.payment_type,
egroup."name" as group_name, course."name" as course_name, payment.status 
from education_grouppayment payment
inner join company_member student on payment.student_id = student.id and student.is_student = true
inner join education_groupstudent grstudent on student.id = grstudent.student_id 
inner join education_group egroup on grstudent.group_id = egroup.id 
inner join education_course course on egroup.course_id = course.id
inner join education_groupperiod grperiod on payment.period_id = grperiod.id
order by grperiod.end_date, payment.status
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_groupperiod")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])



def _format(data):
    if data["student_id"]:
        student = OrderedDict([
            ('id', data['student_id']),
            ('lastname', data["lastname"]),
            ('firstname', data['firstname'])])
    else:
        student = None


    return OrderedDict([
        ('student', student),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('amount', data['amount']),
        ('payment_date', data['payment_date']),
        ('payment_type', data['payment_type']),
        ('group_name', data['group_name']),
        ('course_name', data['course_name']),
        ('status', data['status'])

    ])
