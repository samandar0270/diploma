from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from .services import monitoring_data


class MonitoringView(GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        result = monitoring_data(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

