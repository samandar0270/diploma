from rest_framework import serializers
from crm.education.models import GroupPeriod


class GrPeriodSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['group_id'] = validated_data.pop("group")
        root = GroupPeriod(**validated_data)
        root.save()
        return root

    class Meta:
        model = GroupPeriod
        exclude = ['id']
