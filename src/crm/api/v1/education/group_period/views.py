from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound

from crm.education.models import GroupPeriod
from .services import one_grperiod, list_grperiod, list_month_period
from .serializers import GrPeriodSerializer


class GrPeriodView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = GrPeriodSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = GroupPeriod.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found group_period')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_grperiod(request, kwargs['pk'])
        elif "m_id" in kwargs and kwargs['m_id']:
            result = list_month_period(request, kwargs['m_id'])
        else:
            result = list_grperiod(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_grperiod(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def put(self, request, *args, **kwargs):
        root = self.get_object(*args, **kwargs)
        serializer = self.get_serializer(data=request.data, instance=root, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        result = one_grperiod(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def delete(self, request, *args, **kwargs):
        root = self.get_object(*args, **kwargs)
        root.active_status = 3
        root.save()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
