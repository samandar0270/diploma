# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator

PER_PAGE = settings.PAGINATE_BY


def list_grperiod(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
        select gr_period.id, gr_period.start_date, gr_period.end_date, edu_gr."name" as group_name, 
        gr_period.group_id, 
        gr_period.price, gr_period.order_month
        from education_groupperiod gr_period
        inner join education_group edu_gr on gr_period.group_id = edu_gr.id
        order by gr_period.start_date desc
            
        limit %s OFFSET %s
        """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt from education_groupperiod")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])
def list_month_period(request, root_id):
    if len(str(root_id))==2:
        month = str(root_id)
    else:
        month = f"'0{root_id}'"
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
        select periodd.id, student.lastname, student.firstname, egroup."name" as group_name, egroup.id as group_id,
        teacher1.lastname as t1_lastname, teacher1.firstname as t1_firstname, teacher2.lastname as t2_lastname, teacher2.firstname as t2_firstname,
        periodd.start_date, periodd.end_date, payment.payment_date, periodd.price, payment.amount, (periodd.price - payment.amount) as debt
        from education_groupperiod periodd
        left join education_group egroup on periodd.group_id = egroup.id
        inner join education_grouppayment payment on periodd.id = payment.period_id
        inner join company_member student on payment.student_id = student.id
        left join company_member teacher1 on egroup.teacher_1_id = teacher1.id and teacher1.is_student = false
        left join company_member teacher2 on egroup.teacher_2_id = teacher2.id and teacher2.is_student = false
        where to_char( periodd.start_date, 'MM')= {}
        order by periodd.id asc    
        limit {} OFFSET {}
        """.format(month, PER_PAGE, offset)
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(month_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            f""" select count(1) as cnt 
                from education_groupperiod periodd
                where to_char( periodd.start_date, 'MM')= {month} """
        )
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    data = total_items(month)
    total_prices = OrderedDict([
        ('total_price', data['total_price']),
        ('total_sum', data['total_sum']),
        ('total_debt', data['total_debt'])
    ])
    result.append(total_prices)
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_grperiod(request, root_id):
    extra_sql = """
    select gr_period.id, gr_period.start_date, gr_period.end_date, edu_gr."name" as group_name, gr_period.group_id, 
    gr_period.price, gr_period.order_month
    from education_groupperiod gr_period
    inner join education_group edu_gr on gr_period.group_id = edu_gr.id 
    where edu_gr.id = %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [root_id])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    return OrderedDict([
        ('item', result),
    ])


def total_items(month):

    extra_sql = """
    select  sum(periodd.price) as total_price, sum(payment.amount) as total_sum, sum(periodd.price - payment.amount) as total_debt
    from education_groupperiod periodd
    inner join education_grouppayment payment on periodd.id= payment.period_id
    where to_char( periodd.start_date, 'MM')= {}
    """.format(month)

    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql)
        data = dictfetchone(cursor)

    return data





def _format(data):
    if data['group_id']:
        group = OrderedDict([
            ('id', data['group_id']),
            ('group_name', data['group_name'])
        ])
    else:
        group = None
    return OrderedDict([
        ('id', data['id']),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('price', data['price']),
        ('order_month', data['order_month']),
        ('group', group)
    ])
def month_format(data):


    return OrderedDict([
        ('id', data['id']),
        ('lastname', data['lastname']),
        ('firstname', data['firstname']),
        ('t1_lastname', data['t1_lastname']),
        ('t1_firstname', data['t1_firstname']),
        ('t2_lastname', data['t2_lastname']),
        ('t2_firstname', data['t2_firstname']),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('payment_date', data['payment_date']),
        ('price', data['price']),
        ('amount', data['amount']),
        ('debt', data['debt']),
        ('group_id', data['group_id']),
        ('group_name', data['group_name'])
    ])

