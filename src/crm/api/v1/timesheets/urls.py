from django.urls import path

from crm.api.v1.timesheets.times.views import TimesView, EventsView

urlpatterns = [
    path("timesheet/", TimesView.as_view(), name="times_list"),
    path("timesheet/<int:pk>/", TimesView.as_view(), name="one_time"),
    path("times/", EventsView.as_view(), name="event_time"),
]