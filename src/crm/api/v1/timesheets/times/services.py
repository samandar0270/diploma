# -*- coding: utf-8 -*-
from contextlib import closing
from collections import OrderedDict
from django.db import connection
from django.conf import settings
from .....base.utils.db import dictfetchall, dictfetchone
from .....base.utils.sqlpaginator import SqlPaginator
from .....education import PaymentChoice

PER_PAGE = settings.PAGINATE_BY

def times_info(request, student_id, date):
    extra_sql = """
    select edu_group."name" as group_name,  gr_period.end_date, payment.status, payment.amount, gr_period.*,
    student.id as student_id, edu_group.id as group_id,
    coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as student
    from education_groupperiod gr_period
    left join education_grouppayment payment on gr_period.id = payment.period_id
    left join education_group edu_group on gr_period.group_id = edu_group.id
    left join company_member student on payment.student_id = student.id
    where student.id = %s and %s  between gr_period.start_date and gr_period.end_date
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id, date])
        data = dictfetchone(cursor)
        if data:
            result = _format_times(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format_times(data):
    if data['group_id']:
        group = OrderedDict([
            ('id', data['group_id']),
            ('group_name', data['group_name'])
        ])
    else:
        group = None

    if data['student_id']:
        student = OrderedDict([
            ('id', data['student_id']),
            ('name', data['student'])
        ])
    else:
        student = None

    if data['status']:
        status = OrderedDict([
            ('id', data['status']),
            ('name', PaymentChoice.getValue(data["status"]))
        ])
    else:
        status = None

    return OrderedDict([
        ('student', student),
        ('group', group),
        ('id', data['id']),
        ('start_date', data['start_date']),
        ('end_date', data['end_date']),
        ('order_month', data['order_month']),
        ('status', status),
        ('amount', data['amount']),
        ('price', data['price']),
    ])


def list_times(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
    select times.id, times.time_date, times.in_time, times.out_time,
    times.member_id,
    coalesce(members.lastname, '') ||' ' ||coalesce(members.firstname, '') as member_name
    from timesheet_times times
    inner join company_member members on times.member_id = members.id
    order by times.in_time desc
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)
        result = []
        for data in items:
            result.append(_format(data))

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt FROM timesheet_times ")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    paginator = SqlPaginator(request, page=page, per_page=PER_PAGE, count=count_records)
    pagging = paginator.get_paginated_response()
    return OrderedDict([
        ('items', result),
        ('meta', pagging)
    ])


def one_times(request, root_id):
    extra_sql = """
    select times.id, times.time_date, times.in_time, times.out_time,
    times.member_id,
    coalesce(members.lastname, '') ||' ' ||coalesce(members.firstname, '') as member_name
    from timesheet_times times
    inner join company_member members on times.member_id = members.id
    where times.id = %s

"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [root_id])
        data = dictfetchone(cursor)
        if data:
            result = _format(data)
        else:
            result = None
    return OrderedDict([
        ('item', result),
    ])


def _format(data):
    if data['member_id']:
        member = OrderedDict([
            ('id', data['member_id']),
            ('name', data['member_name'])
        ])
    else:
        member = None
    return OrderedDict([
        ('id', data['id']),
        ('time_date', data['time_date']),
        ('in_time', data['in_time']),
        ('out_time', data['out_time']),
        ('member', member),
        ])
