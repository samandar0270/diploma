from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from crm.timesheet.models import Times
from .services import list_times, one_times, times_info
from .serializers import TimesSerializer, EventsSerializer


class TimesView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = TimesSerializer

    def get_object(self, *args, **kwargs):
        try:
            product = Times.objects.get(id=kwargs['pk'])
        except Exception as e:
            raise NotFound('not found Timesheet')
        return product

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs and kwargs['pk']:
            result = one_times(request, kwargs['pk'])
        else:
            result = list_times(request)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.create(serializer.data)
        result = one_times(request, data.pk)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')


class EventsView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = EventsSerializer
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        time_model = serializer.create(serializer.data)
        date = time_model.time_date.strftime("%Y-%m-%d")
        result = times_info(request, time_model.member_id, date)
        return Response(result, status=status.HTTP_200_OK, content_type='application/json')

