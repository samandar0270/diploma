class PaymentType:
    CHOICES = ((1, 'Naqd'), (2, 'Terminal'), (3, 'Pul o`tkazish'), (4, 'Online'))

    @classmethod
    def getValue(self, index):
        result = '-'
        for i, name in self.CHOICES:
            if i == index:
                result = name
        return result
