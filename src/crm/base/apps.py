from django.apps import AppConfig


class BaseConfig(AppConfig):
    name = 'crm.base'
