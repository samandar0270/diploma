import logging

from django.core.paginator import InvalidPage, Paginator
from django.http import Http404
from versatileimagefield.image_warmer import VersatileImageFieldWarmer

from .paginator import sqlPaginator

logger = logging.getLogger(__name__)

def sql_paginator_items(items, count, paginate_by, page_number):
    if not page_number:
        page_number = 1
    paginator = sqlPaginator(items, count, paginate_by)
    try:
        page_number = int(page_number)
    except ValueError:
        raise Http404("Page can not be converted to an int.")

    try:
        items = paginator.page(page_number)
    except InvalidPage as err:
        raise Http404(
            "Invalid page (%(page_number)s): %(message)s"
            % {"page_number": page_number, "message": str(err)}
        )
    return items

def get_paginator_items(items, paginate_by, page_number):
    if not page_number:
        page_number = 1
    paginator = Paginator(items, paginate_by)
    try:
        page_number = int(page_number)
    except ValueError:
        raise Http404("Page can not be converted to an int.")

    try:
        items = paginator.page(page_number)
    except InvalidPage as err:
        raise Http404(
            "Invalid page (%(page_number)s): %(message)s"
            % {"page_number": page_number, "message": str(err)}
        )
    return items

def create_thumbnails(pk, model, size_set, image_attr=None):
    instance = model.objects.get(pk=pk)
    if not image_attr:
        image_attr = "image"
    image_instance = getattr(instance, image_attr)
    if image_instance.name == "":
        # There is no file, skip processing
        return
    warmer = VersatileImageFieldWarmer(
        instance_or_queryset=instance, rendition_key_set=size_set, image_attr=image_attr
    )
    logger.info("Creating thumbnails for  %s", pk)
    num_created, failed_to_create = warmer.warm()
    if num_created:
        logger.info("Created %d thumbnails", num_created)
    if failed_to_create:
        logger.error("Failed to generate thumbnails", extra={"paths": failed_to_create})