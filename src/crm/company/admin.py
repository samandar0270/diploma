from django.contrib import admin
from .models import Profile

class ProfileAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "phone_number", "region")

admin.site.register(Profile, ProfileAdmin)
