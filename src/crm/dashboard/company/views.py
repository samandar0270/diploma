from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.company.models import (Position, Profile, Member)
from ..views import staff_member_required
from .forms import (PositionForm, ProfileForm, EmployeeForm, StudentForm)

from .services import (list_position, list_employee,)


@staff_member_required
def company_edit(request):
    try:
        company_model = Profile.objects.order_by('id')[0:1].get()
    except Exception:
        company_model = None

    form = ProfileForm(
        request.POST or None,
        request.FILES or None,
        instance=company_model
    )

    if form.is_valid():
        company_model = form.save()
        return redirect("dashboard:company-profile")


    ctx = {
        "form": form,
        "company_model": company_model,
        "section": 'company',
        "active":"4",
    }
    return TemplateResponse(request, "dashboard/company/profile/form.html", ctx)

@staff_member_required
def company_profile(request):
    pk=request.user.company_id
    try:
        company_model = get_object_or_404(Profile, pk=pk)
    except Exception:
        company_model = None

    ctx = {
        "company_model": company_model,
        "section": 'company',
        "active": "4",
    }
    return TemplateResponse(request, "dashboard/company/profile/detail.html", ctx)


@staff_member_required
def position_list(request):
    pk=request.user.company_id
    positions = list_position(request, pk)
    list = sql_paginator_items(
        positions['items'], positions['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )

    ctx = {
        "positions": positions['items'],
        "list": list,
        "section": 'company',
        "active": "5",
    }
    return TemplateResponse(request, "dashboard/company/position/list.html", ctx)


@staff_member_required
def position_create(request):
    pk=request.user.company_id
    company = Profile.objects.get(pk=pk)
    position = Position()
    form = PositionForm(request.POST or None, request.FILES or None,{'company':company})
    if form.is_valid():
        position = form.save()
        return redirect("dashboard:position-list")
    ctx = {"position": position, "form": form, "active":"5","section":"company"}
    print(form.errors)
    return TemplateResponse(request, "dashboard/company/position/form.html", ctx)


@staff_member_required
def position_edit(request, pk):
    company_id = request.user.company_id
    company = Profile.objects.get(pk=company_id)
    position = get_object_or_404(Position, pk=pk)
    form = PositionForm(
        request.POST or None,
        request.FILES or None,
        {'company': company},
        instance=position
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:position-list")
    elif form.errors:
        status = 400
    ctx = {"position": position, "form": form,"section":"company","active":"5"}
    template = "dashboard/company/position/form.html"
    return TemplateResponse(request, template, ctx, status=status)


@staff_member_required
def position_details(request, pk):
    position = get_object_or_404(Position, pk=pk)
    ctx = {
        "position": position,
        "active": "5",
        "section":"company",
    }
    return TemplateResponse(request, "dashboard/company/position/detail.html", ctx)


@staff_member_required
def position_delete(request, pk):
    position = get_object_or_404(Position, pk=pk)
    position.delete()
    return redirect("dashboard:position-list")


@staff_member_required
def employee_list(request):
    pk=request.user.company_id
    employees = list_employee(request, pk)
    list = sql_paginator_items(
        employees['items'], employees['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )

    ctx = {
        "employees": employees['items'],
        "list": list,
        "section": 'company',
        "active": "6",
    }
    return TemplateResponse(request, "dashboard/company/employee/list.html", ctx)

@staff_member_required
def employee_create(request):
    employee = Member()
    pk = request.user.company_id
    company = Profile.objects.get(pk=pk)
    position=Position.objects.filter(company_id=pk)
    form = EmployeeForm(request.POST or None, request.FILES or None,{'company':company})
    if form.is_valid():
        form.save()
        return redirect("dashboard:employee-list")
    print(form.errors)
    ctx = {"employee": employee, "form": form,"active":"6","section":"company",'position':position}
    return TemplateResponse(request, "dashboard/company/employee/form.html", ctx)

@staff_member_required
def employee_edit(request, pk):
    company_id = request.user.company_id
    company = Profile.objects.get(pk=company_id)
    position = Position.objects.filter(company_id=company_id)
    employee = get_object_or_404(Member, pk=pk)
    form = EmployeeForm(
        request.POST or None,
        request.FILES or None,
        {'company': company},
        instance=employee
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:employee-list")
    elif form.errors:
        status = 400
    ctx = {"employee": employee, "form": form,"active":"6","section":"company",'position':position}
    template = "dashboard/company/employee/form.html"
    return TemplateResponse(request, template, ctx, status=status)

@staff_member_required
def employee_details(request, pk):
    employee = get_object_or_404(Member, pk=pk)
    ctx = {
        "employee": employee,
        "active": "6",
        "section": "company",
    }
    return TemplateResponse(request, "dashboard/company/employee/detail.html", ctx)

@staff_member_required
def employee_delete(request, pk):
    employee = get_object_or_404(Member, pk=pk)
    employee.delete()
    return redirect("dashboard:employee-list")