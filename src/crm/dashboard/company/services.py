# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings
from ...base.utils.db import dictfetchone, dictfetchall

PER_PAGE = settings.DASHBOARD_PAGINATE_BY

def list_position(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_position.id, company_position.title, company_position.status
FROM company_position
WHERE company_position.company_id = %s
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_position")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def list_employee(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,company_member.status,
company_position.title as position_name
FROM company_member
left join company_position on company_member.position_id = company_position.id
where company_member.status = 1 or company_member.status = 2 and company_member.is_student = false and company_member.company_id = %s
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = false")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}





