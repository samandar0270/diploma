from django.urls import path

from . import views

urlpatterns = [
    path("profile/", views.company_profile, name="company-profile"),
    path("edit/", views.company_edit, name="company-edit"),
    path("positions/", views.position_list, name="position-list"),
    path('positions/<int:pk>/info/', views.position_details, name='position-details'),
    path('positions/<int:pk>/edit/', views.position_edit, name='position-edit'),
    path('positions/<int:pk>/delete/', views.position_delete, name='position-delete'),
    path('positions/add/', views.position_create, name='position-add'),
    path("employees/", views.employee_list, name="employee-list"),
    path('employees/<int:pk>/info/', views.employee_details, name='employee-details'),
    path('employees/<int:pk>/edit/', views.employee_edit, name='employee-edit'),
    path('employees/<int:pk>/delete/', views.employee_delete, name='employee-delete'),
    path('employees/add/', views.employee_create, name='employee-add'),
]
