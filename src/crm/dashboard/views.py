from django.template.response import TemplateResponse
from django.contrib import auth, messages
from django.contrib.auth import views as django_views
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.admin.views.decorators import (
    staff_member_required as _staff_member_required,
)
from .services import groups_count, courses_count, students_count

from .forms import LoginForm


def staff_member_required(f):
    return _staff_member_required(f, login_url="dashboard:login")


@staff_member_required
def index(request):
    ctx = {
        'company': 1,
        'section': "home",
        'courses_count': courses_count,
        'groups_count': groups_count,
        'students_count': students_count

    }
    return TemplateResponse(request, "dashboard/index.html", ctx)


def login(request):
    if request.method == 'GET' and request.user.is_active and request.user.is_staff:
        # Already logged-in, redirect to admin index
        index_path = reverse('dashboard:dashboard_index', current_app='dashboard')
        return HttpResponseRedirect(index_path)

    kwargs = {"template_name": "dashboard/login.html", "authentication_form": LoginForm}
    return django_views.LoginView.as_view(**kwargs)(request, **kwargs)


@login_required
def logout(request):
    auth.logout(request)
    messages.success(request, "You have been successfully logged out.")
    return redirect('dashboard:dashboard_index', )
