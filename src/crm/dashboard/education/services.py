# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings
from ...base.utils.db import dictfetchone, dictfetchall

PER_PAGE = settings.DASHBOARD_PAGINATE_BY

def teacher_student_data(request,teacher_id,order_month,company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
select students.id as student_id,teachers.id as teacher_id,coalesce(teachers.lastname, '') ||' ' ||coalesce(teachers.firstname, '') as teacher , ec."name" as course_name,ed_group."name" as group_name,
coalesce(students.lastname, '') ||' ' ||coalesce(students.firstname) as student, gr_payment.amount ,gr_payment .payment_date ,gr_payment.price_month,ed_group.id as group_id
from education_grouppayment gr_payment
inner join company_member students on gr_payment.student_id = students.id and students.company_id = {company_id} and students.is_student = True
inner join education_groupperiod gr_period on gr_payment.period_id =  gr_period.id and to_char( gr_period.start_date, 'MM')= %s
inner join education_group ed_group on gr_period.group_id = ed_group.id
inner join education_course ec on ec.id = ed_group.course_id and ec.company_id = {company_id}
inner join company_member teachers on ed_group.teacher_1_id = teachers.id or ed_group.teacher_2_id = teachers.id
where  teachers.id = {teacher_id} and teachers.company_id = {company_id} and teachers.is_student = False
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [order_month, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt FROM education_groupperiod""")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def teacher_all(request,company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
    SELECT *
    FROM company_member cm
    where cm.company_id = {company_id} and cm.is_student = False
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt FROM company_member where company_member.is_student = False")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def list_monitoring(request, group_id, company_id, order_month):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
select cm.id, coalesce(cm.lastname, '') ||' ' ||coalesce(cm.firstname, '') as full_name, eg."name" as group_name,
payment.period_id,payment.status, gr_period.start_date || ' || ' ||gr_period.end_date as payment_period, ec.name as course_name,
payment.payment_date , payment.payment_type,payment.amount
from education_groupperiod gr_period 
inner join education_group eg on gr_period.group_id = eg.id
inner join education_grouppayment payment on gr_period.id = payment.period_id
inner join company_member cm on payment.student_id = cm.id
inner join education_course ec on ec.id = eg.course_id
where eg.id = {group_id} and to_char( gr_period .start_date, 'MM')= %s and payment.company_id = {company_id}
order by gr_period.end_date, payment.status 
limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [order_month,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM education_grouppayment")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def groups_students_delete(student_id):

    extra_sql = """
select eg3.id
from education_groupstudent eg 
inner join education_groupperiod eg2 on eg2.group_id = eg.group_id
inner join education_grouppayment eg3 on eg3.period_id = eg2.id
where eg.id = %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id])
        items = dictfetchall(cursor)
    return items

def listgroup_period(request, order_month, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
select eg2."name" as group_name, eg.start_date , eg.end_date,teachers.teacher_1,teachers.teacher_2,student.full_name,eg3.amount,eg3.price_month ,
student.id_student,eg3.payment_date,eg2.id as group_id 
from education_groupperiod eg 
inner join education_group eg2 on eg2.id=eg.group_id
inner join education_grouppayment eg3 on eg3.period_id = eg.id
left join(
select eg5.id as group_id,coalesce(cm3.lastname, '') ||' ' ||coalesce(cm3.firstname, '') as teacher_1,
coalesce(cm4.lastname, '') ||' ' ||coalesce(cm4.firstname, '') as teacher_2
from education_group eg5
inner join company_member cm3 ON cm3.id=eg5.teacher_1_id 
inner join company_member cm4 ON cm4.id=eg5.teacher_2_id 
where cm3.company_id ={company_id} and cm4.company_id ={company_id} and cm3.is_student =false and cm4.is_student = False
) as teachers on teachers.group_id=eg.group_id
left join(
select coalesce(cm.lastname, '') ||' ' ||coalesce(cm.firstname, '') as full_name, eg.group_id as group_id,cm.id as id_student,cm.company_id 
from company_member cm
inner join education_groupstudent eg on eg.student_id = cm.id 
where cm.is_student = true and cm.company_id = {company_id}
) as student on student.group_id=eg.group_id and  student.id_student=eg3.student_id
where to_char( eg.start_date, 'MM')= %s and eg3.company_id={company_id} and student.company_id={company_id}
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [order_month, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt FROM education_groupperiod""")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def list_groupperiod(request, group_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT *
FROM education_groupperiod
where group_id = %s
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [group_id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt FROM education_groupperiod""")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def list_groupperiod_id(request, id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT *
FROM education_groupperiod
where id = %s
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt FROM education_groupperiod""")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def list_course(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
SELECT education_course.id, education_course.name, education_course.price,education_course.active_status,education_course.company_id
FROM education_course
WHERE education_course.company_id = %s
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM education_course")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}





def list_group(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT eg.id as group_id, eg."name" , eg.start_date, eg.end_date, eg.start_time, eg.end_time, coalesce(cm.lastname, '') ||' ' ||coalesce(cm.firstname, '') as teacher_1, coalesce(cm2.lastname, '') ||' ' ||coalesce(cm2.firstname, '') as teacher_2,eg.active_status  
FROM education_group eg
inner join company_member cm ON eg.teacher_1_id = cm.id  
left join company_member cm2 ON eg.teacher_2_id = cm2.id
inner join education_course ec on ec.id=eg.course_id 
where ec.company_id = %s
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM education_group")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}





def list_employee(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
company_position.title as position_name
FROM company_member
left join company_position on company_member.position_id = company_position.id
where company_member.status = 1 or company_member.status = 2 and company_member.is_student = false
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = false")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def list_students(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
company_member.join_date, company_member.birthday, geo_region.name as region_name
FROM company_member
left join geo_region on company_member.region_id = geo_region.id
where company_member.status = 1 or company_member.status = 2 and company_member.is_student = true
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def list_students_group(request, group_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT members.id as member_id, coalesce(members.lastname, '') ||' ' ||coalesce(members.firstname, '') as full_name,
students.start_date, students.active_status,students.id as groupstudent_id
FROM education_groupstudent students
inner join education_group eg on students.group_id = eg.id
inner join company_member members on students.student_id = members.id
where students.group_id = %s
order by students.start_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [group_id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt
FROM education_groupstudent students
inner join education_group eg on students.group_id = eg.id
inner join company_member members on students.student_id = members.id
where students.group_id = %s
""", [group_id])
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}



def groups_students(student_id):
    extra_sql = """
SELECT eg.id as group_id, eg."name" , eg.active_status, eg.period_type, eg.price_month
FROM education_groupstudent students
inner join education_group eg on students.group_id = eg.id
where students.student_id = %s
order by students.start_date desc
limit 20
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id])
        items = dictfetchall(cursor)
    return items


def education_group(request, id):

    extra_sql = """
    select *
    from education_group eg 
    where eg.id = %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [id])
        items = dictfetchall(cursor)
    return items[0]