from django.conf import settings
from datetime import datetime
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.education.models import Group, GroupStudent,GroupPayment
from crm.company.models import Member
from crm.education.models import Course
from ..views import staff_member_required
from .forms import CourseForm, GroupForm, GroupStudentForm
from .services import list_group, list_students_group,list_groupperiod,listgroup_period,groups_students_delete,\
    list_monitoring,list_groupperiod_id,teacher_all,education_group,teacher_student_data

@staff_member_required
def teachers(request):
    company_id=request.user.company_id
    teacher=teacher_all(request,company_id)
    ctx = {
        "section":"teachers",
        "teacher":teacher['items'],
        'data':None
    }
    return TemplateResponse(request, "dashboard/education/teacher_data/list.html", ctx)

@staff_member_required
def teachers_data(request, pk):
    time = datetime.now()
    id = str(time.month)
    if len(id) == 2:
        id = id
    else:
        id = f'0{time.month}'
    company_id = request.user.company_id
    teacher = teacher_all(request, company_id)
    teacher_data= teacher_student_data(request,pk,id,company_id)
    summa=[]
    b={}
    x=0
    y=0
    z=0
    for data in teacher_data['items']:
        y+=int(data['price_month'])
        z+=int(data['amount'])
        if int(data['price_month'])>int(data['amount']):
            x=int(data['price_month'])-int(data['amount'])
        elif int(data['price_month'])<int(data['amount']):
            x=int(data['amount'])-int(data['price_month'])
            x=f"Ortiqcha summa {x}"
        elif int(data['price_month'])==int(data['amount']):
            x=None
        b['group_id']=data['group_id']
        b['student_id']=data['student_id']
        b['qarz']=x
        summa.append(b)
        b={}
    qarz_2=0
    if y>z:
        qarz_2=y-z
    elif y<z:
        qarz_2=f"Ortiqcha summa {z-y}"
    elif y==z:
        qarz_2=None
    ctx = {
        "section": "teachers",
        "teacher": teacher['items'],
        'data': 'teacher',
        "teacher_data":teacher_data['items'],
        "id":pk,
        "summa":summa,
        "y":y,
        "z":z,
        "qarz_2":qarz_2,
    }
    return TemplateResponse(request, "dashboard/education/teacher_data/list.html", ctx)


@staff_member_required
def payment_period(request):
    dict=[
        {
            'id':1,
            'oy':'yanvar'
        },
        {
            'id':2,
            'oy': 'fevral'
        },
        {
            'id': 3,
            'oy': 'mart'
        },
        {
            'id': 4,
            'oy': 'aprel'
        },
        {
            'id': 5,
            'oy': 'may'
        },
        {
            'id': 6,
            'oy': 'iyun'
        },
        {
            'id': 7,
            'oy': 'iyul'
        },
        {
            'id': 8,
            'oy': 'august'
        },
        {
            'id': 9,
            'oy': 'sentabr'
        },
        {
            'id': 10,
            'oy': 'oktabr'
        },
        {
            'id': 11,
            'oy': 'noyabr'
        },
        {
            'id': 12,
            'oy': 'dekabr'
        },

    ]

    ctx = {
        "dict":dict,
        "section":"period",
        "money":None
    }
    return TemplateResponse(request, "dashboard/education/payment/list.html", ctx)

def period_list_month(request, pk):
    company_id=request.user.company_id
    dict = [
        {
            'id': 1,
            'oy': 'yanvar'
        },
        {
            'id': 2,
            'oy': 'fevral'
        },
        {
            'id': 3,
            'oy': 'mart'
        },
        {
            'id': 4,
            'oy': 'aprel'
        },
        {
            'id': 5,
            'oy': 'may'
        },
        {
            'id': 6,
            'oy': 'iyun'
        },
        {
            'id': 7,
            'oy': 'iyul'
        },
        {
            'id': 8,
            'oy': 'august'
        },
        {
            'id': 9,
            'oy': 'sentabr'
        },
        {
            'id': 10,
            'oy': 'oktabr'
        },
        {
            'id': 11,
            'oy': 'noyabr'
        },
        {
            'id': 12,
            'oy': 'dekabr'
        },

    ]
    id=str(pk)
    if len(id)==2:
        id=id
    else:
        id=f'0{pk}'
    students = listgroup_period(request, id,company_id)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    debt=[]
    b={}
    money_month=0
    money_amount=0
    for data in students['items']:
        money_month+=int(data['price_month'])
        money_amount+=int(data['amount'])
    for data in students['items']:
        x=data['id_student']
        z=data['group_id']
        y=int(data['price_month'])-int(data['amount'])
        if y<0:
            y=int(data['amount'])-int(data['price_month'])
            y=f"Ortiqcha summa {y} ming so'm"
        b['id']=x
        b['group_id'] = z
        b['amount']=y
        debt.append(b)
        b={}
    if money_month>money_amount:
        qarz=money_month-money_amount
    else:
        qarz=None
    ctx = {
        "students": students['items'],
        "list": list,
        "section": 'period',
        "dict":dict,
        "money":"money",
        "debt":debt,
        "id":pk,
        "money_month":money_month,
        "money_amount":money_amount,
        "qarz":qarz
    }
    return TemplateResponse(request, "dashboard/education/payment/list.html", ctx)

@staff_member_required
def add_student_group(request, pk):
    price_group=education_group(request,pk)
    initial={
        'price_group':price_group['price_month']
    }
    company=request.user.company_id
    group = Group.objects.get(pk = pk)
    form = GroupStudentForm(request.POST or None, request.FILES or None, {"group":group,"company":company},initial=initial)
    student=Member.objects.filter(is_student=True).filter(company_id=company)
    students=Member.objects.filter(is_student=True).filter(company_id=company)
    ctx = {
        "form": form,
        'group': None,
        "section": 'education',
        "student": student,
        "students": students,
        }
    if form.is_valid():
        try:
            form.save()
            return redirect("dashboard:group-details", pk)
        except Exception as e:
            ctx["except"] = "ERROR"

    return TemplateResponse(request, "dashboard/education/group_student/form.html", ctx)

@staff_member_required
def lists(request):
    pk=request.user.company_id
    groups = list_group(request, pk)
    list = sql_paginator_items(
        groups['items'], groups['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )

    ctx = {
        "group": groups['items'],
        "list": list,
        "section": 'group'
    }
    return TemplateResponse(request, "dashboard/education/group/list.html", ctx)

@staff_member_required
def creates(request):
    pk=request.user.company_id
    course=Course.objects.filter(company_id=pk)
    member=Member.objects.filter(is_student=False).filter(company_id=pk)
    form = GroupForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect("dashboard:group-list")
    ctx = {"form": form, 'group': None, "section": 'education',"courses":course,"member":member}
    return TemplateResponse(request, "dashboard/education/group/form.html", ctx)

@staff_member_required
def edits(request, pk):
    company_id = request.user.company_id
    course = Course.objects.filter(company_id=company_id)
    member = Member.objects.filter(is_student=False).filter(company_id=company_id)
    group = get_object_or_404(Group, pk=pk)
    form = GroupForm(
        request.POST or None,
        request.FILES or None,
        instance=group
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:group-list")
    elif form.errors:
        status = 400
    ctx = {"group": group, "form": form, "active":"2", "section":"education","courses":course,"member":member}
    template = "dashboard/education/group/form.html"
    return TemplateResponse(request, template, ctx, status=status)

@staff_member_required
def details(request, pk):
    company_id=request.user.company_id
    group = get_object_or_404(Group, pk=pk)
    date=datetime.now()
    x=''
    if len(str(date.month))==1:
        x=f'0{date.month}'
    else:
        x=str(date.month)
    monitoring=list_monitoring(request,pk,company_id,x)
    student = list_students_group(request, group.id)
    groupperiod_data=list_groupperiod(request, group.id)
    groupperiod = sql_paginator_items(groupperiod_data['items'], groupperiod_data['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page"))
    students = sql_paginator_items(
        student['items'], student['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "group": group,
        "students": students,
        "section": 'education',
        "active": "2",
        "groupperiod":groupperiod,
        "monitoring":monitoring['items'],
    }

    return TemplateResponse(request, "dashboard/education/group/detail.html", ctx)

@staff_member_required
def details_monitoring(request, pk):
    period=list_groupperiod_id(request,pk)
    pk=period['items'][0]['group_id']
    if len(str(period['items'][0]['start_date'].month))==1:
        x=f"0{period['items'][0]['start_date'].month}"
    else:
        x=str(period['items'][0]['start_date'].month)
    company_id=request.user.company_id
    group = get_object_or_404(Group, pk=pk)
    monitoring=list_monitoring(request,pk,company_id,x)
    student = list_students_group(request, group.id)
    groupperiod_data=list_groupperiod(request, group.id)
    groupperiod = sql_paginator_items(groupperiod_data['items'], groupperiod_data['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page"))
    students = sql_paginator_items(
        student['items'], student['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "group": group,
        "students": students,
        "section": 'education',
        "active": "2",
        "groupperiod":groupperiod,
        "monitoring":monitoring['items'],
    }

    return TemplateResponse(request, "dashboard/education/group/detail.html", ctx)

@staff_member_required
def deletes(request, pk):
    group = get_object_or_404(Group, pk=pk)
    group.delete()
    return redirect("dashboard:group-list")

@staff_member_required
def groupstudent_delete(request,pk):
    data=groups_students_delete(pk)
    for i in data:
        grouppayment=get_object_or_404(GroupPayment, pk=int(i['id']))
        grouppayment.delete()
    groupstudent=get_object_or_404(GroupStudent, pk=pk)
    groupstudent.delete()
    return redirect("dashboard:group-list")
