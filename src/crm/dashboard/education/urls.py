from django.urls import path

from . import course_views, group_views, billing_views


urlpatterns = [
    path("courses/", course_views.lists, name="course-list"),
    path('courses/<int:pk>/info/', course_views.details, name='course-details'),
    path('courses/<int:pk>/edit/', course_views.edits, name='course-edit'),
    path('courses/<int:pk>/delete/', course_views.deletes, name='course-delete'),
    path('courses/add/', course_views.creates, name='course-add'),

    path("groups/", group_views.lists, name="group-list"),
    path('groups/<int:pk>/info/', group_views.details, name='group-details'),
    path('groups/<int:pk>/info/period/', group_views.details_monitoring, name='group-details-id'),
    path('groups/<int:pk>/edit/', group_views.edits, name='group-edit'),
    path('groups/<int:pk>/delete/', group_views.deletes, name='group-delete'),
    path('groupsstudent/<int:pk>/delete/', group_views.groupstudent_delete, name='groupstudent-delete'),
    path('groups/add/', group_views.creates, name='group-add'),

    path("group/student/<int:pk>/add/", group_views.add_student_group, name="add-student-group"),
    path("payment/<int:pk>/group/", billing_views.edits, name="add-payment-group"),
    path("edit/<int:pk>/group/", billing_views.edits, name="edit-payment-group"),

    path("payment/period/group/", group_views.payment_period, name="payment-period"),
    path("payment/period/<int:pk>/group/", group_views.period_list_month, name="period-list-month"),

    path("teacher/", group_views.teachers, name="teacher"),
    path("teacher/<int:pk>/data/", group_views.teachers_data, name="teachers-data"),
]
