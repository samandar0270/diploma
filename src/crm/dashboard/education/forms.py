from django import forms
from crm.education.models import (Course, Group, GroupStudent, GroupPeriod, GroupPayment)
from crm.education.actions import PeriodManager, PaymentManager
from ..widgets import DatePicker, TimePicker
from .services import groups_students
from crm.settings import TIME_INPUT_FORMAT


class CourseForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = args[-1].get('company')
        super().__init__(*args, **kwargs)


    def save(self, commit=True):
        self.instance.company = self.company
        super().save()
        return self.instance


    class Meta:
        model = Course
        fields = '__all__'
        labels = {
            "name": "Kurs nomi",
            "price": "Kurs narxi",
        }

class GroupForm(forms.ModelForm):
    start_date = forms.DateField(
        help_text="Kursning boshlanish kuni",
        required=True,
        label="Boshlanish kuni",
        widget=DatePicker,
    )

    end_date = forms.DateField(
        help_text="Kursning so'nggi kunni",
        required=True,
        label="Tugash kuni",
        widget=DatePicker,
    )

    start_time = forms.TimeField(
        help_text="Darsning boshlanish vaqti",
        required=True,
        label="Boshlanish vaqti",
        widget=TimePicker,
    )

    end_time = forms.TimeField(
        help_text="Darsning tugash vaqti",
        required=True,
        label="Tugash vaqti",
        widget=TimePicker,
    )

    def save(self, commit=True):
        data = self.cleaned_data['active_status']
        if data == 2:
            super().save()
            has_period = GroupPeriod.objects.filter(group_id=self.instance.id).exists()
            if not has_period:
                manager = PeriodManager()
                manager.createPeriod(self.instance)

            return self.instance
        else:
            super().save()

    class Meta:
        model = Group
        exclude = ["schedule"]
        labels = {
            "name": "Guruh nomi",
            "start_date": "Boshlanish sanasi",
            "end_date": "Tugash sanasi",
            "start_time": "Boshanish vaqti",
            "end_time": "Tugash vaqti",
            "teacher_1": "1 - O'qituvchi",
            "teacher_2": "2 - O'qituvchi",
            "period_type": "Yangi oyning boshlanishi",
            "price_month": "Narxi",
            "course": "Kurs nomi",
        }

class GroupStudentForm(forms.ModelForm):
    start_date = forms.DateField(
        help_text="Kursning boshlanish kuni",
        required=True,
        label="Boshlanish kuni",
        widget=DatePicker,
    )

    end_date = forms.DateField(
        help_text="Kursning so'nggi kunni",
        required=False,
        label="Tugash kuni",
        widget=DatePicker,
    )

    def __init__(self, *args, **kwargs):
        self.group = args[-1].get('group')
        self.company = args[-1].get('company')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        self.instance.group = self.group
        super().save()
        manager = PaymentManager()
        manager.createGroupPayment(self.instance, self.company,self.instance.price_group)
        return self.instance

    class Meta:
        model = GroupStudent
        fields = '__all__'
        exclude = ["group"]
        labels = {
            "student": "Student",
            "start_date": "O'qishni boshlagan vaqti",
            "end_date": "O'qishni tugatish vaqti",
        }


class PaymentForm(forms.ModelForm):
    group = forms.ChoiceField(required=True, label='Guruh')
    period = forms.IntegerField(required=True, label='Period', widget=forms.Select)
    payment_date = forms.DateField(
        required=True,
        label="To'lov sanasi",
        widget=DatePicker
    )
    payment_time = forms.TimeField(
        required=True,
        label="To'lov sanasi",
        widget=TimePicker
    )



    def __init__(self, *args, **kwargs):
        self.student = kwargs.pop("student")
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.instance.student = self.student
        groups = groups_students(self.student.id)
        choices = []
        choices.append(('', '----'))
        for data in groups:
            choices.append((data['group_id'], data['name']))
        self.fields['group'].choices = choices

    def clean_period(self):
        data = self.cleaned_data['period']
        period = GroupPeriod.objects.get(id=data)
        self.instance.period = period

        return period

    def save(self, commit=True):
        data = self.cleaned_data
        try:
            model = GroupPayment.objects.filter(student_id=self.instance.student_id, period=data['period']).get()
            amount = model.amount + data['amount']
        except Exception:
            model = GroupPayment(student_id=self.instance.student_id)
            amount = data['amount']
        model.period = data['period']
        model.amount = amount
        model.payment_type = data['payment_type']
        model.status = data['status']
        model.payment_date = data['payment_date']
        model.payment_time = data['payment_time']
        model.save()
        return model

    class Meta:
        model = GroupPayment
        exclude = ["student"]
        labels = {
            "period": "period",
            "student": "student",
            "payment_date": "payment_date",
            "payment_time": "payment_time",
            "amount": "amount",
            "payment_type": "payment_type",
            "status": "status"
        }


