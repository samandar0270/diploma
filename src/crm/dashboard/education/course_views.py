from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.education.models import Course
from ..views import staff_member_required
from .forms import CourseForm
from .services import list_course
from crm.company.models import Profile


@staff_member_required
def lists(request):
    pk=request.user.company_id
    courses = list_course(request, pk)
    list = sql_paginator_items(
        courses['items'], courses['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "courses": courses['items'],
        "list": list,
        "section": 'course'
    }
    return TemplateResponse(request, "dashboard/education/course/list.html", ctx)

@staff_member_required
def creates(request):
    pk=request.user.company_id
    company = Profile.objects.get(pk=pk)
    form = CourseForm(request.POST or None, request.FILES or None,{'company':company})
    if form.is_valid():
        form.save()
        return redirect("dashboard:course-list")
    print(form.errors)
    ctx = {"form": form, 'course': None, "section": 'education',"active": '1'}
    return TemplateResponse(request, "dashboard/education/course/form.html", ctx)

@staff_member_required
def edits(request, pk):
    company_id = request.user.company_id
    company = Profile.objects.get(pk=company_id)
    course = get_object_or_404(Course, pk=pk)
    form = CourseForm(
        request.POST or None,
        request.FILES or None,
        {'company': company},
        instance=course
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:course-list")
    elif form.errors:
        status = 400
    ctx = {"course": course, "form": form, "section": 'education', "active": '1'}
    template = "dashboard/education/course/form.html"
    return TemplateResponse(request, template, ctx, status=status)

@staff_member_required
def details(request, pk):
    course = get_object_or_404(Course, pk=pk)
    ctx = {
        "course": course,
        "section": 'education',
        "active": '1'
    }
    return TemplateResponse(request, "dashboard/education/course/detail.html", ctx)

@staff_member_required
def deletes(request, pk):
    course = get_object_or_404(Course, pk=pk)
    course.delete()
    return redirect("dashboard:course-list")
    # if request.method == "POST":
    #     course.active_status = 3
    #     course.save()
    #
    #     if request.is_ajax():
    #         response = {"redirectUrl": reverse("dashboard:course-list")}
    #         return JsonResponse(response)
    #     return redirect("dashboard:course-list")
    # ctx = {
    #     "course": course,
    #     "section": 'education',
    #     "active": '1'
    # }
    # return TemplateResponse(
    #     request, "dashboard/company/student/modal/confirm_delete.html", ctx
    # )
