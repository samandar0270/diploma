from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.education.models import Group, GroupStudent,GroupPayment
from crm.company.models import Member
from ..views import staff_member_required
from .forms import PaymentForm
from .services import list_group, list_students_group

@staff_member_required
def add_payment_group(request, pk):
    student = get_object_or_404(Member, pk=pk)
    form = PaymentForm(request.POST or None,  student=student)
    if form.is_valid():
        form.save()
        return redirect("dashboard:student-details", pk)
    ctx = {"form": form, 'group': None, "section": 'education'}
    return TemplateResponse(request, "dashboard/education/payment/form.html", ctx)

@staff_member_required
def edits(request, pk):
    student = get_object_or_404(Member, pk=pk)
    form = PaymentForm(
        request.POST or None,
        request.FILES or None,
        student=student,
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:student-list")
    elif form.errors:
        status = 400
    ctx = {"form": form, 'group': None, "section": 'education'}
    return TemplateResponse(request, "dashboard/education/payment/form.html", ctx)




