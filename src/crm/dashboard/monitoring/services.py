# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings
from ...base.utils.db import dictfetchone, dictfetchall

PER_PAGE = settings.DASHBOARD_PAGINATE_BY

def monitoring(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
select cm.id, coalesce(cm.lastname, '') ||' ' ||coalesce(cm.firstname, '') as full_name, eg."name" as group_name,
payment.period_id,payment.status, gr_period.start_date || ' || ' ||gr_period.end_date as payment_period, ec.name as course_name,
payment.payment_date , payment.payment_type,payment.amount
from education_groupperiod gr_period 
inner join education_group eg on gr_period.group_id = eg.id
inner join education_grouppayment payment on gr_period.id = payment.period_id
inner join company_member cm on payment.student_id = cm.id
inner join education_course ec on ec.id = eg.course_id
order by gr_period.end_date, payment.status 
limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM education_grouppayment")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

