from PIL import Image,ImageFont,ImageDraw
from django.conf import settings
import pyqrcode
import qrcode
from django.http import JsonResponse,HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.company.models import Member
from ..views import staff_member_required
from .services import monitoring



@staff_member_required
def lists(request):
    students = monitoring(request)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "monitoring": students['items'],
        "list": list,
        "section": 'monitoring',
    }
    return TemplateResponse(request, "dashboard/monitoring/list.html", ctx)



