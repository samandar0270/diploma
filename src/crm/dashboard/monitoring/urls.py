from django.urls import path

from . import views

urlpatterns = [
    path("", views.lists, name="monitoring"),
    # path('<int:pk>/info/', views.details, name='student-details'),
    # path('<int:pk>/edit/', views.edits, name='student-edit'),
    # path('<int:pk>/delete/', views.deletes, name='student-delete'),
    # path('add/', views.creates, name='student-add'),
    # path('qrcode/<int:pk>/generator', views.qrcode_generator, name='qrcode_generator'),
]
