# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def dictfetchone(cursor):
    row = cursor.fetchone()
    if row is None:
        return False
    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, row))


def courses_count():
    sql = """
        select count(id) from education_course
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(sql)
        items = dictfetchone(cursor)
    return items


def groups_count():
    sql = """
        select count(id) from education_group
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(sql)
        items = dictfetchone(cursor)
    return items


def students_count():
    sql = """
        select count(id) from company_member
        where is_student = True
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(sql)
        items = dictfetchone(cursor)
    return items
