from django.conf import settings
from django.http import JsonResponse
from crm.company.models import Member
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from ...base.utils import sql_paginator_items
from ..views import staff_member_required
from .forms import TimesForm
from ...timesheet.models import Times
from .services import list_times,list_times_delete

@staff_member_required
def lists(request):
    pk=request.user.company_id
    times = list_times(request, pk)
    list = sql_paginator_items(
        times['items'], times['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "times": times['items'],
        "list": list,
        "section": 'times',
        "active": '1'
    }
    return TemplateResponse(request, "dashboard/timesheet/times/list.html", ctx)


@staff_member_required
def creates(request):
    pk=request.user.company_id
    students=Member.objects.filter(is_student=True).filter(company_id=pk)
    form = TimesForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("dashboard:times-list")
    ctx = {"form": form, "section": 'times','students':students}
    return TemplateResponse(request, "dashboard/timesheet/times/form.html", ctx)

@staff_member_required
def edits(request, pk):
    times = get_object_or_404(Times, pk=pk)
    form = TimesForm(
        request.POST or None,
        instance=times
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:times-list")
    elif form.errors:
        status = 400
    ctx = {"times": times, "form": form, "active": "2", "section":"times"}
    template = "dashboard/timesheet/times/form.html"
    return TemplateResponse(request, template, ctx, status=status)

@staff_member_required
def delete(request, pk):
    list_times_delete(pk)
    return redirect("dashboard:times-list")
    # times = list_times(request)
    # list = sql_paginator_items(
    #     times['items'], times['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    # )
    # ctx = {
    #     "times": times['items'],
    #     "list": list,
    #     "section": 'times',
    #     "active": '1'
    # }
    # return TemplateResponse(request, "dashboard/timesheet/times/list.html", ctx)