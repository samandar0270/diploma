# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings
from ...base.utils.db import dictfetchone, dictfetchall

PER_PAGE = settings.DASHBOARD_PAGINATE_BY

def list_times_delete(pk):
    extra_sql = """
    DELETE FROM timesheet_times WHERE id= %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [pk])


def list_times(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
select times.id,  times.time_date, times.in_time, times.out_time, members.id as idd, coalesce(members.lastname, '') ||' ' ||coalesce(members.firstname, '') as full_name
FROM timesheet_times times
inner join company_member members on times.member_id = members.id
WHERE members.company_id = %s
ORDER BY times.time_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("select count(1) as cnt FROM timesheet_times times")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}



