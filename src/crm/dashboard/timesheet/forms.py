from django import forms
from crm.timesheet.models import Times
from ..widgets import DatePicker, TimePicker

class TimesForm(forms.ModelForm):
    time_date = forms.DateField(
        help_text="Markazga kelgan kun",
        required=True,
        label="Kun",
        widget=DatePicker,
    )

    in_time = forms.TimeField(
        help_text="Kelgan soati",
        required=True,
        label="Kelgan vaqt",
        widget=TimePicker,
    )

    out_time = forms.TimeField(
        help_text="Ketgan soati",
        required=True,
        label="Ketgan vaqt",
        widget=TimePicker,
    )


    class Meta:
        model = Times
        fields = '__all__'
        labels = {
            "member": "Shaxs",
            "time_date": "Kun",
            "in_time": "Kelgan vaqt",
            "out_time": "Ketgan vaqt",
        }
