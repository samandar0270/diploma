from django.urls import path

from . import views

urlpatterns = [
    path("times/", views.lists, name="times-list"),
    path('times/add/', views.creates, name='times-add'),
    path('times/<int:pk>/edit/', views.edits, name='times-edit'),
    path('times/<int:pk>/delete/', views.delete, name='times-delete'),
]
