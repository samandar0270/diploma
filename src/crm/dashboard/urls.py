from . import views as core_views
from django.urls import path, include
app_name = 'dashboard'
urlpatterns = [
        path('', core_views.index, name='dashboard_index'),
        path('dashboard/', core_views.index, name='dashboard_index'),
        path('login/', core_views.login, name='login'),
        path('logout/', core_views.logout, name='logout'),
        path('company/', include('crm.dashboard.company.urls')),
        path('education/', include('crm.dashboard.education.urls')),
        path('timesheet/', include('crm.dashboard.timesheet.urls')),
        path('students/', include('crm.dashboard.students.urls')),
        path('monitoring/', include('crm.dashboard.monitoring.urls')),
]
