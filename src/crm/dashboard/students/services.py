# -*- coding: utf-8 -*-
from contextlib import closing
from django.db import connection
from django.conf import settings
from ...base.utils.db import dictfetchone, dictfetchall

PER_PAGE = settings.DASHBOARD_PAGINATE_BY

def pechat_student_data(request, payment_id,company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
    select perriod.start_date, perriod.end_date, student.id as student_id, coalesce(student.lastname, '') ||' ' ||coalesce(student.firstname, '') as full_name,coalesce(teacher.lastname, '') ||' ' ||coalesce(teacher.firstname, '') as teacher_name,
    payment.amount, payment.payment_type, payment.payment_date, payment.payment_time, eg."name" as group_name,ec."name" as course_name
    from education_grouppayment payment 
    inner join education_groupperiod perriod on payment.period_id = perriod.id
    inner join education_group eg on eg.id = perriod.group_id 
    right join company_member teacher on eg.teacher_1_id = teacher.id 
    right join company_member student on payment.student_id = student.id
    inner join education_course ec on ec.id = eg.course_id and ec.company_id = {company_id}
    where payment.id= %s and payment.company_id = {company_id}
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [payment_id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def student_search_list_data(request, student_name,company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
    SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
    company_member.join_date, company_member.birthday, geo_region.name as region_name
    FROM company_member
    left join geo_region on company_member.region_id = geo_region.id
    where (company_member.status = 1 or company_member.status = 2) and company_member.is_student = true and company_member.company_id = {company_id} and company_member.firstname ILIKE %s 
    order by join_date desc
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_name, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}


def list_students2(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
company_member.join_date, company_member.birthday, geo_region.name as region_name, eg2."name" as group_name, company_member.phone_number
FROM company_member
left join geo_region on company_member.region_id = geo_region.id
inner join education_groupstudent eg on company_member.id= eg.student_id 
inner join education_group eg2 on eg2.id=eg.group_id 
where (company_member.status = 4 or company_member.status = 1) and company_member.is_student = true and company_member.status=4
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 4 and company_member.is_student = true")
        row = dictfetchone(cursor)
    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def course_student_group(request, course_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = f"""
    select cm.id, coalesce(cm.lastname, '') ||' ' ||coalesce(cm.firstname, '') as full_name,
    cm.join_date, cm.birthday, geo_region.name as region_name, eg2."name" as group_name,cm.phone_number
    from company_member cm 
   left join geo_region on cm.region_id = geo_region.id
   inner join education_groupstudent eg ON cm.id = eg.student_id
   inner join education_group eg2 on eg2.id = eg.group_id 
   inner join education_course ec on ec.id = eg2.course_id
    where cm.is_student = true and cm.status = 4 and eg2.active_status = 4 and ec.id = {course_id}  
    limit %s OFFSET %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute(
            "SELECT count(1) as cnt FROM company_member where status = 1 or status = 4 and company_member.is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def student_payment_period(student_id, period_id):
    try:
        with closing(connection.cursor()) as cursor:
            sql = f"""
                  select eg3.amount, eg2."name" as group_name,eg2.price_month, ec."name" as course_name, eg2.id as group_id, eg3.period_id 
                  from education_groupstudent eg 
                  inner join education_group eg2 on eg2.id=eg.group_id 
                  inner join education_grouppayment eg3 on eg3.student_id = eg.student_id 
                  inner join education_course ec on ec.id=eg2.course_id 
                  where eg.student_id ={student_id} and eg3.period_id = {period_id}         
                  """
            cursor.execute(sql)
            student = dictfetchall(cursor)
        if student!=[]:
            return student
        else:
            return student
    except:
        return []



def studentgroupperiod(group_id):
    try:
        with closing(connection.cursor()) as cursor:
            sql = f"""
                  select eg6.start_date, eg6.end_date , eg6.group_id,eg6.order_month 
                   from education_groupperiod eg6 
                   where eg6.group_id = {group_id}             
            """
            cursor.execute(sql)
            student = dictfetchall(cursor)
        if student!=[]:
            return student
        else:
            return student
    except:
        return []


def studentgroup_info(student_id):
    try:
        with closing(connection.cursor()) as cursor:
            sql = f"""
                  select * from education_groupstudent
                  where student_id = {student_id}            
            """
            cursor.execute(sql)
            student = dictfetchone(cursor)
        if student!=[]:
            return student
        else:
            return student
    except:
        return []


def students_delete(student_id):
    extra_sql = """
    DELETE FROM company_member WHERE id= %s
    """
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id])

def list_position(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_position.id, company_position.title, company_position.status
FROM company_position
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_position")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

def list_employee(request):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
company_position.title as position_name
FROM company_member
left join company_position on company_member.position_id = company_position.id
where company_member.status = 1 or company_member.status = 2 and company_member.is_student = false
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = false")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}







def list_groups_forstudents(request, student_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT eg.id as group_id, eg."name" , eg.start_date, eg.end_date, eg.start_time, eg.end_time 
FROM education_groupstudent students
inner join education_group eg on students.group_id = eg.id
where students.student_id = %s
order by students.start_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id, PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""SELECT count(1) as cnt
FROM education_groupstudent students
inner join company_member members on students.student_id = members.id
inner join education_group eg on students.group_id = eg.id
where students.group_id = %s""", [student_id])
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}






def list_students(request, company_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
SELECT company_member.id, coalesce(company_member.lastname, '') ||' ' ||coalesce(company_member.firstname, '') as full_name,
company_member.join_date, company_member.birthday, geo_region.name as region_name
FROM company_member
left join geo_region on company_member.region_id = geo_region.id
where (company_member.status = 1 or company_member.status = 2) and company_member.is_student = true and company_member.company_id = %s
order by join_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [company_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("SELECT count(1) as cnt FROM company_member where status = 1 or status = 2 and company_member.is_student = true")
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}
def student_info(pk):
    try:
        with closing(connection.cursor()) as cursor:
            sql = f"""
                  select * from company_member
                  where is_student = true and id = {pk}            
            """
            cursor.execute(sql)
            student = dictfetchone(cursor)
        return student
    except:
        return []

def list_payments(request, student_id):
    try:
        page = int(request.GET.get('page', 1))
    except:
        page = 1
    offset = (page - 1) * PER_PAGE

    extra_sql = """
select payment.id as id, payment.student_id as student, perriod.start_date, perriod.end_date, payment.amount, payment.payment_type, payment.payment_date, eg."name" 
from education_grouppayment payment 
inner join education_groupperiod perriod on payment.period_id = perriod.id
inner join education_group eg on eg.id = perriod.group_id 
where payment.student_id = %s
order by payment_date desc
limit %s OFFSET %s
"""
    with closing(connection.cursor()) as cursor:
        cursor.execute(extra_sql, [student_id,PER_PAGE, offset])
        items = dictfetchall(cursor)

    with closing(connection.cursor()) as cursor:
        cursor.execute("""select count(1) as cnt
from education_grouppayment payment 
inner join education_groupperiod perriod on payment.period_id = perriod.id
inner join education_group eg on eg.id = perriod.group_id 
where payment.student_id = %s""", [student_id])
        row = dictfetchone(cursor)

    if row:
        count_records = row['cnt']
    else:
        count_records = 0

    return {'items': items, 'count': count_records}

