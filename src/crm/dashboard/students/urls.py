from django.urls import path

from . import views

urlpatterns = [
    path("", views.lists, name="student-list"),
    path('<int:pk>/info/', views.details, name='student-details'),
    path('<int:pk>/edit/', views.edits, name='student-edit'),
    path("student/list", views.lists2, name="student-list2"),
    path("course/<int:pk>/student", views.course_students, name="course-student-list"),
    path('<int:pk>/delete/', views.deletes, name='student-delete'),
    path('add/', views.creates, name='student-add'),
    path('qrcode/<int:pk>/generator', views.qrcode_generator, name='qrcode_generator'),

    path('student/search', views.student_search_list, name='student-list-search'),
    path('student/payment/<int:pk>/pechat/', views.pechat_student_payment, name='payment-student-pechat'),
]
