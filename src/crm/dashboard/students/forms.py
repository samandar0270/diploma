from django import forms
from crm.base import GenderChoice
from crm.company.models import Position, Profile, Member
from crm.geo.models import District
from ..widgets import DatePicker


class QrcodeForm(forms.Form):
    img=forms.ImageField(label="Rasmni kiriting:")

class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields['region'].initial = None
        self.fields['district'].queryset = self.district_queryset()

    def district_queryset(self):
        if self.instance.pk is None:
            return District.objects.all()

        return District.objects.filter(region_id=self.instance.region_id)

    class Meta:
        model = Profile
        fields = ['title', 'email', 'description', 'phone_number', 'address', 'region', 'district']
        labels = {
            "title": "Markaz Nomi",
            "email": "Email",
            "description": "Qo‘shimcha ma'lumot",
            "phone_number": "Telefon raqam",
            "address": "Manzil",
            "region": "Viloyat/shahar",
            "district": "Tuman",
        }

class PositionForm(forms.ModelForm):
    class Meta:
        model = Position
        fields = ['title', 'status']
        labels = {
            "title": 'Название',
            "status": 'Статус',
        }

class EmployeeForm(forms.ModelForm):

    gender = forms.ChoiceField(required=True, label='Jins', choices=GenderChoice.CHOICES)

    birthday = forms.DateField(
        help_text="Tug'ilgan kuni",
        required=True,
        label="Tug'ilgan kuni",
        widget=DatePicker,
    )

    join_date = forms.DateField(
        help_text="Kelgan vaqti",
        required=True,
        label="Kelgan vaqti",
        widget=DatePicker,
    )

    def __init__(self, *args, **kwargs):

        super(EmployeeForm, self).__init__(*args, **kwargs)

        self.fields['region'].initial = None
        self.fields['district'].queryset = self.district_queryset()

    def district_queryset(self):
        if self.instance.pk is None:
            return District.objects.all()

        return District.objects.filter(region_id=self.instance.region_id)

    def save(self, commit=True):
        all_value = self.cleaned_data
        self.instance.gender = True if all_value['gender'] == '1' else False
        instance = super().save(commit=commit)
        return instance

    class Meta:
        model = Member
        fields = '__all__'
        # exclude = ['join_date', 'is_student']
        labels = {
            "firstname": "Ism",
            "lastname": "Familiya",
            "middlename": "Otasini ismi",
            "email": "Email",
            "phone_number": "Telefon raqam",
            "birthday": "Tug‘ilgan kun",
            "gender": "Jisni",
            "address": "Manzil",
            "pass_serial": "Passport ma'lumoti",
            "position": "Lavozim",
            "status": "Status",
            "region": "Viloyat/shahar",
            "district": "Tuman",
            "join_date": "Kelgan vaqt",
        }

class StudentForm(forms.ModelForm):

    gender = forms.ChoiceField(required=True, label='Jins', choices=GenderChoice.CHOICES)

    birthday = forms.DateField(
        help_text="Tug'ilgan kuni",
        required=True,
        label="Tug'ilgan kuni",
        widget=DatePicker,
    )

    join_date = forms.DateField(
        help_text="Kelgan vaqti",
        required=True,
        label="Kelgan vaqti",
        widget=DatePicker,
    )

    def __init__(self, *args, **kwargs):
        self.company = args[-1].get('company')
        super(StudentForm, self).__init__(*args, **kwargs)

        self.fields['region'].initial = None
        self.fields['district'].queryset = self.district_queryset()

    def district_queryset(self):
        if self.instance.pk is None:
            return District.objects.all()

        return District.objects.filter(region_id=self.instance.region_id)

    def save(self, commit=True):
        all_value = self.cleaned_data
        self.instance.gender = True if all_value['gender'] == "1" else False
        self.instance.is_student = True
        self.instance.company = self.company
        instance = super().save(commit=commit)
        return instance

    class Meta:
        model = Member
        # fields = '__all__'
        exclude = ['position', 'is_student']
        labels = {
            "firstname": "Ism",
            "lastname": "Familiya",
            "middlename": "Otasini ismi",
            "email": "Email",
            "phone_number": "Telefon raqam",
            "birthday": "Tug‘ilgan kun",
            "gender": "Jisni",
            "address": "Manzil",
            "pass_serial": "Passport ma'lumoti",
            "position": "Lavozim",
            "status": "Status",
            "region": "Viloyat/shahar",
            "district": "Tuman",
            "join_date": "Kelgan vaqt",
        }