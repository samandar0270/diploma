from PIL import Image,ImageFont,ImageDraw
from io import BytesIO
from django.conf import settings
import qrcode
import os
import datetime
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse,HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.response import TemplateResponse
from django.urls import reverse
from ...base.utils import sql_paginator_items
from crm.company.models import Member
from crm.education.models import Course
from ..views import staff_member_required
from .forms import StudentForm,QrcodeForm
from .services import list_students, list_groups_forstudents, list_payments, student_search_list_data,pechat_student_data,\
    student_info,students_delete,studentgroup_info,studentgroupperiod,student_payment_period,list_students2,course_student_group
from crm.dashboard.education.services import list_course
from pathlib import Path
from crm.company.models import Profile

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent

@staff_member_required
def student_search_list(request):
    company_id = request.user.company_id
    student_name=request.POST['table_search']
    students=student_search_list_data(request,student_name,company_id)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "students": students['items'],
        "list": list,
        "section": 'student',
    }
    return TemplateResponse(request, "dashboard/student/list.html", ctx)


@staff_member_required
def lists2(request):
    pk=request.user.company_id
    course=list_course(request, pk)
    students = list_students2(request)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "courses":course['items'],
        "students": students['items'],
        "list": list,
        "section": 'talaba',
    }
    return TemplateResponse(request, "dashboard/student/list2.html", ctx)


@staff_member_required
def course_students(request, pk):
    company_id=request.user.company_id
    kurs = Course.objects.filter(pk=pk)
    course = list_course(request,company_id)
    students = course_student_group(request, pk)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "course":kurs[0],
        "courses": course['items'],
        "students": students['items'],
        "list": list,
        "section": 'talaba',
    }
    return TemplateResponse(request, "dashboard/student/list2.html", ctx)


@staff_member_required
def lists(request):
    pk=request.user.company_id
    students = list_students(request, pk)
    list = sql_paginator_items(
        students['items'], students['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    ctx = {
        "students": students['items'],
        "list": list,
        "section": 'student',
    }
    return TemplateResponse(request, "dashboard/student/list.html", ctx)


@staff_member_required
def qrcode_generator(request, pk):
    student  = student_info(pk)
    qr = qrcode.QRCode(
        version=1,
        box_size=10,
        border=4
    )
    data = f"{pk}"
    qr.add_data(data)
    qr.make()
    img = qr.make_image(fill_color="white", back_color="black")
    img.save(f"student_{pk}.png")

    my_image = Image.open(f"student_{pk}.png")
    title_text = f"{student['firstname']} {student['lastname']}"
    image_editable = ImageDraw.Draw(my_image)
    fontsize = 1
    img_fraction = 0.50
    font_path = "arial.ttf"
    font = ImageFont.truetype(font_path, fontsize)
    while font.getsize(title_text)[0] < img_fraction * my_image.size[0]:
        fontsize += 1
        font = ImageFont.truetype(font_path, fontsize)
    fontsize -= 1
    font = ImageFont.truetype(font_path, fontsize)
    image_editable.text((55, 265), title_text, font=font)

    with BytesIO() as image_binary:
        my_image.save(image_binary, 'PNG')
        image_binary.seek(0)
        return HttpResponse(image_binary, content_type="image/png")


@staff_member_required
def creates(request):
    pk = request.user.company_id
    company = Profile.objects.get(pk=pk)
    form = StudentForm(request.POST or None, request.FILES or None,{'company':company})
    if form.is_valid():
        form.save()
        return redirect("dashboard:student-list")
    ctx = {
        "form": form,
        "section": 'student'}
    return TemplateResponse(request, "dashboard/student/form.html", ctx)

@staff_member_required
def edits(request, pk):
    company_id = request.user.company_id
    company = Profile.objects.get(pk=company_id)
    student = get_object_or_404(Member, pk=pk)
    form = StudentForm(
        request.POST or None,
        request.FILES or None,
        {'company': company},
        instance=student
    )
    status = 200

    if form.is_valid():
        form.save()
        return redirect("dashboard:student-list")
    elif form.errors:
        status = 400
    ctx = {"student": student, "form": form, "section": 'student'}
    template = "dashboard/student/form.html"
    return TemplateResponse(request, template, ctx, status=status)

@staff_member_required
def details(request, pk):
    student = get_object_or_404(Member, pk=pk)
    groups = list_groups_forstudents(request, pk)
    payment = list_payments(request, pk)
    payments = sql_paginator_items(
        payment['items'], payment['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )
    list = sql_paginator_items(
        groups['items'], groups['count'], settings.DASHBOARD_PAGINATE_BY, request.GET.get("page")
    )

    ctx = {
        "student": student,
        "section": 'student',
        "groups": groups['items'],
        "list": list,
        "payments":payments

    }
    return TemplateResponse(request, "dashboard/student/detail.html", ctx)

@staff_member_required
def pechat_student_payment(request,pk):
    company_id = request.user.company_id
    patment_data=pechat_student_data(request,pk,company_id)
    ctx = {
        "payments": patment_data['items']
    }
    return TemplateResponse(request, "dashboard/student/payment_print.html", ctx)


@staff_member_required
def deletes(request, pk):
    student = get_object_or_404(Member, pk=pk)
    student.delete()
    return redirect("dashboard:student-list")


