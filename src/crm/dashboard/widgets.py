from django.forms import DateInput, TimeInput

class DatePicker(DateInput):
    template_name = "dashboard/widgets/datepicker.html"


class TimePicker(TimeInput):
    template_name = "dashboard/widgets/timepicker.html"

