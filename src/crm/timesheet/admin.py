from django.contrib import admin
from .models import Times

class TimesAdmin(admin.ModelAdmin):
    list_display = ("member", "time_date", "in_time", "out_time")

admin.site.register(Times, TimesAdmin)
