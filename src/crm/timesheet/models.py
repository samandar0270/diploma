from django.db import models
from crm.company.models import Member


class Times(models.Model):
    member = models.ForeignKey(Member, related_name='memebertimes', blank=False, null=False, on_delete=models.CASCADE,limit_choices_to={'is_student':True},)
    time_date = models.DateField(blank=False, null=False)
    in_time = models.TimeField(blank=False, null=False)
    out_time = models.TimeField(blank=True, null=True)

    def __str__(self):
        return str(self.in_time)

    class Meta:
        verbose_name = 'Keldi ketti'
        verbose_name_plural = 'Vaqt Jadvali'
