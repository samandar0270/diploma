from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'crm.user'
