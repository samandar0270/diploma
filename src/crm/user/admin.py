from django.contrib import admin
from .models import User

class userAdmin(admin.ModelAdmin):
    list_display = ("email", "firstname",  "lastname", "date_joined", "email")
    list_display_links = ('email', )

admin.site.register(User, userAdmin)
