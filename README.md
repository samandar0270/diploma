## Loyiha haqida

Ushbu Loyiha o‘quv markazlari uchun ma'lum bir jarayonlarni avtomatlashtirish uchun kerak bo‘ladi.


## Loyihani qilishdagi texnik tomonlar

- **Python**: 3.8 versiyada qilinmoqda. 
- **PostgreSql**: Ma'lumotlar bazasi PostgreSqlning 12 versiyasida.
- **Django**: Django Frameworkning 3.2 versiyasi orqali qilinmoqda.
